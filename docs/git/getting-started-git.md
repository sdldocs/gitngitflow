
이 페이지에서는 Git을 사용하는 방법을 간략하게 소개한다. 즉 Git에서 자주 사용하는 명령어를 설명한다. 이 페이지를 끝내면 

- [Git을 설치하는 방법](#installing-git),
- [Git 설정 방법](#first-time-git-setup),
- [저장소를 만들고 설정하는 방법](#getting-git-repostory), 
- [파일을 추적(track)하거나 하지 않는 방법](#recording-changes-to-repository)과 
- [변경 내용을 stage하고 커밋하는 방법](#recording-changes-to-repository)을 알수 있다. 

또한 [파일이나 파일 패턴을 무시하도록 Git을 설정하는 방법](#ignoring-files), [실수를 쉽고 빠르게 만회하는 방법](#undoing-things), [프로젝트 히스토리를 조회하고 커밋을 비교하는 방법](#viewing-commit-history), [리모트 저장소에 push하고 pull하는 방법](#remote-repository) 등을 살펴본다.

## Git 설치 <a id="installing-git"></a>
Git을 사용하기에 앞서 설치해야 한다. 

**Note**: 이 페이지는 Git 2.25.1 버전을 기준으로 작성되었다. 대부분의 명령어는 그 이전 버전에서도 잘 동작하지만, 몇 가지 기능은 아예 없거나 미묘하게 다를 수 있다. Git의 하위 호환성은 정말 훌륭하기 때문에 2.25 이후 버전에서도 잘 동작할 것이다.

### Linux에 설치 <a id="installing-onn-linux"></a>
Linux에서 패키지로 Git을 설치할 때는 보통 각 배포판에서 사용하는 패키지 관리도구를 사용하여 설치한다. Fedora (또는 비슷하게 RPM 기반 패키지 시스템을 사용하는 RHEL, CentOS)에서는 아래와 같이 `dnf`를 사용 한다.
```shell
$ sudo dnf install git-all
```

Ubuntu등의 데비안 계열 배포판에서는 `apt`를 사용한다.
```shell
$ sudo apt install git-all
```

### Mac에 설치 <a id="installing-on-mac"></a>
Mac에 Git을 설치하는 방법 중에는 Xcode Command Line Tools를 설치하는 방법이 가장 쉽다. Mavericks(10.9)부터는 Terminal에 단지 처음으로 'git’을 실행하는 것으로 설치가 시작된다. 'git’이 설치돼 있지 않으면 설치하라고 안내해 준다.
```bash
$ git --version
```

만약 Git이 시스템에 설치되어있지 않은 경우, 설치할 수 있도록 안내 메시지가 뜰 것이다.

### Window에 설치 <a id="installing-on-window"></a>
여러 방법으로 Windows에 Git을 설치할 수 있다. 공식 배포판은 Git 웹사이트에서 내려받을 수 있다. [http://git-scm.com/download/win](http://git-scm.com/download/win)에 가면 자동으로 다운로드가 시작된다. 이는 'Git for Windows’ 프로젝트인데, Git 자체와는 다른 별도의 프로젝트이다. 자세한 정보는 [https://gitforwindows.org](https://gitforwindows.org/)에서 확인할 수 있다.

자동화된 설치 방식을 [Git Chocolatey 패키지](https://community.chocolatey.org/packages?q=GIT)에서 살펴볼 수 있다. 패키지는 커뮤니티에 의해 운영되는 프로그램이다.

이제 여러분의 컴퓨터에 Git를 설치했다면 이제 본격적으로 Git의 사용법에 대하여 살펴보자.

## Git 최초 설정 <a id="first-time-git-setup"></a>
Git을 설치하고 나면 Git의 사용 환경을 적절하게 설정해 주어야 한다. 환경 설정은 한 컴퓨터에서 한 번만 하면 된다. 설정한 내용은 Git을 업그레이드해도 유지된다. 언제든지 다시 바꿀 수 있는 명령어도 있다.

`git config` 명령으로 설정 내용을 확인하고 변경할 수 있다. Git은 이 설정에 따라 동작한다. 이때 아래 세 설정 파일을 사용한다.

1. `/etc/gitconfig` 파일: 시스템의 모든 사용자와 모든 repository에 적용되는 설정이다. `git config --system` 옵션으로 이 파일을 읽고 쓸 수 있다. (이 파일은 시스템 전체 설정 파일이기 때문에 수정하려면 시스템의 관리자 권한이 필요하다.)
2. `~/.gitconfig` 파일: 특정 사용자(즉 현재 사용자)에게만 적용되는 설정이다. `git config --global` 옵션으로 이 파일을 읽고 쓸 수 있다. 특정 사용자의 모든 repository 설정에 적용된다.
3. `.git/config` : 이 파일은 Git 디렉토리에 있고 특정 저장소(혹은 현재 작업 중인 프로젝트)에만 적용된다. `--local` 옵션을 사용하면 이 파일을 사용하도록 지정할 수 있다. 하지만 기본적으로 이 옵션이 적용되어 있다. (당연히, 이 옵션을 적용하려면 Git repository인 디렉토리로 이동 한 후 적용할 수 있다.)

각 설정은 역순으로 우선시 된다. 그래서 `.git/config` 가 `~/.gitconfig`보다 우선한다.

Windows에서는 $HOME 디렉토리에서 `.gitconfig` 파일을 찾는다 (아마도 C:\Users\$USER 디렉토리). Windows에서도 `[path]/etc/gitconfig`에서 찾는다. 이 경로는 아마도 MSys 루트의 상대경로일 것이므로, MSys 루트는 인스톨러로 Git을 Windows에 설치할 때 결정된다. 이 시스템 설정 파일은 `git config -f <file>` 명령으로 변경할 수 있다. 관리자 권한이 필요하다.

모든 설정과 설정 파일의 경로를 아래 명령으로 볼 수 있다.
```shell
$ git config --list --show-origin
```

### 사용자 정보 <a id="user-identity"></a>
Git을 설치하고 나서 가장 먼저 해야 하는 것은 사용자 이름과 이메일 주소를 설정하는 것이다. Git은 커밋할 때마다 이 정보를 사용한다. 한 번 커밋한 후에는 정보를 변경할 수 없다.
```
$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com
```

다시 말하자면 `--global` 옵션 설정은 설치 후 한 번만 하면 된다. 해당 시스템에서 해당 사용자로 사용할 때 이 정보를 사용한다. 만약 프로젝트마다 다른 이름과 이메일 주소를 사용하고 싶다면 --global 옵션을 빼고 명령을 실행한다.

GUI 도구들은 처음 실행할 때 이 설정을 묻는다.

### 편집기 <a id="editor"></a>
사용자 정보를 설정하고 나면 Git에서 사용할 텍스트 편집기를 고른다. 기본적으로 Git은 시스템의 기본 편집기를 사용한다.

하지만, Emacs 같은 다른 편집기를 사용할 수 있고 아래와 같이 실행하면 된다.
```shell
$ git config --global core.editor emacs
```

Windows에서는 다른 텍스트 편집기를 사용할 수 있다. 실행 파일의 전체 경로를 설정해주면 된다. 실행 파일의 전체 경로는 사용하는 편집기에 따라 다르다.

Windows 환경에서 많이 사용되는 Notepad 편집기의 경우 주로 32비트 버전을 사용하게 된다. 현재 기준으로 64비트 버전을 사용하면 동작하지 않는 플러그인이 많다. 32비트 Windows 시스템이거나, 64비트 Windows 시스템에서 64비트 Notepad을 설치했다면 다음과 같이 설정한다.
```shell
$ git config --global core.editor "'C:/Program Files/Notepad++/notepad++.exe' -multiInst -nosession"
```

64비트 Windows 시스템에서 32비트 Notepad++을 설치했다면 `C:\Program Files (x86)`에 설치된다.
```shell
$ git config --global core.editor "'C:/Program Files (x86)/Notepad++/notepad++.exe' -multiInst -nosession"
```

**Note**: Vim과 Emacs, Notepad++은 꽤 많이 사용되는 편집기로 개발자들이 즐겨 사용한다. Mac이나 Linux 같은 Unix 시스템, Windows 시스템에서 사용 가능하다. 여기서 소개하는 편집기들이 불편해서 다른 편집기를 사용하고자 한다면 해당 편집기를 Git 편집기로 설정하는 방법을 찾아봐야 한다.

**Warning**: 자신의 편집기를 설정하지 않으면 갑자기 실행된 편집기에 당황할 수 있다. 그땐 당황하지 말고 편집기를 그냥 종료하면 Git 명령을 취소할 수 있다.

### 기본 브랜치 이름 <a id="default-branch-name"></a>
Git가 있는 새 repository를 만들 때 기본적으로 Git는 **main**이라는 브랜치를 만든다<a id="foot-1" href="#foot-1-ref"><sup>1</sup></a>. Git 버전 2.28 이후부터는 초기 브랜치에 다른 이름을 설정할 수 있다.

기본 브랜치 이름으로 **master**을 설정하려면 다음을 수행한다. 
```
$ git config --global init.defaultBranch master
```

**Warning**: 디폴트 브랜치 명으로 `master`는 더 이상 사용하지 않으며, `main`을 사용한다.)

### 설정 확인 <a id="checking-settings"></a>
`git config --list` 명령을 실행하면 설정한 모든 것을 보여주어 바로 확인할 수 있다.
```
$ git config --list
user.name=John Doe
user.email=johndoe@example.com
color.status=auto
color.branch=auto
color.interactive=auto
color.diff=auto
...
```

Git은 같은 키를 여러 파일(`/etc/gitconfig` 와 `~/.gitconfig`)에서 읽기 때문에 같은 키가 여러 개 있을 수도 있다. 그러면 Git은 나중 값을 사용한다.

git config `<key>` 명령으로 Git이 특정 Key에 대해 어떤 값을 사용하는지 확인할 수 있다.
```
$ git config user.name
yoonjoon.lee
```

**Note**: Git이 설정된 값을 읽을 때 여러 파일에서 동일한 키에 대해 다른 값을 설정하고 있을 수 있다. 값이 기대한 값과 다를 수 있는데 값만 보고 쉽게 그 원인을 찾을 수 없다. 이때 키에 설정된 값이 어디에서 설정되었는지 확인할 수 있으며, 다음과 같은 명령으로 어떤 파일로부터 설정된 값인지를 확인할 수 있다.
```
$ git config --show-origin rerere.autoUpdate
file:/home/yoonjoon.lee/.gitconfig	false
```

## Git Repository 생성 <a id="getting-git-repostory"></a>
주로 다음 두 가지 중 한 가지 방법으로 Git local repository 사용을 시작할 수 있다.

1. 아직 버전관리를 하지 않는 로컬 디렉토리 하나를 선택해서 Git repository로 사용하는 방법
1. 다른 어딘가(주로 공유하는 remote repository)에서 Git repository를 clone하는 방법

어떤 방법을 사용하든 로컬 디렉토리에 Git repository가 준비되면 이제 시작할 수 있다.

### 이미 있는 디렉토리를 Git 저장소로 만들기 <a id="init_repository-in-existing-directory"></a>
버전관리를 하지 않았던 기존 프로젝트를 Git으로 관리하고 싶은 경우 우선 프로젝트의 디렉토리로 이동한다. 이러한 과정을 처음 해보는 것이라면 시스템마다 조금 다른 점을 주의해야 한다.

> **Note**: 이후 명령은 Linux 시스템을 기준으로 한다. 보다 정확히는 Ubuntu이다. 만약 Mac인 경우에는 대부분 Linux 명령어를 그대로 사용할 수 있으나 Window의 경우에는 [윈도우즈 10에서 드디어 bash shell을...!!](https://krauser085.github.io/microsoft-loves-linux/)를 참고하여 사용하도록 한다.

```bash
$ cd /home/user/my_project
```

그리고 아래와 같은 명령을 실행한다.
```bash
$ git init
```

이 명령은 `.git`이라는 하위 디렉토리를 만든다. `.git` 디렉토리에는 repository에 필요한 뼈대 파일(Skeleton)이 들어 있다. 이 명령만으로는 아직 프로젝트의 어떤 파일도 관리되지 않는다. (`.git` 디렉토리가 만들어진 직후 정확히 어떤 파일이 있는 지에 대한 내용은 [Git Internals](https://git-scm.com/book/en/v2/Git-Internals-Plumbing-and-Porcelain)에서 살펴볼 수 있다.)

Git이 파일을 관리하게 하려면 저장소에 파일을 추가하고 커밋해야 한다. `git add` 명령으로 파일을 추가하고 `git commit` 명령으로 커밋한다.
```bash
$ git add *.c
$ git add LICENSE   # optional LICENSE 파일
$ git commit -m 'initial project version'
```

위의 명령어로 순식간에 Git repository를 만들고 파일 버전 관리를 시작할 수 있다.

### 기존 Repository를 Clone <a id="cloning-existing-repository"></a>
이미 있는 Git repository를 복사하고 싶을 때, 예를 들면 특정 프로젝트에 참여하려고(contribute) 할 때, `git clone` 명령을 사용한다. 이미 Subversion 같은 VCS에 익숙한 사용자에게는 "checkout" 이 아니라 "clone" 이라는 점이 돋보일 것이다. Git이 Subversion과 다른 가장 큰 차이점은 단순히 working 복사본 대신 서버에 있는 거의 모든 데이터를 복사한다는 것이다. `git clone`을 실행하면 기본적으로 모든 파일의 모든 버전 즉 프로젝트 이력를 전부 받아온다. 실제로 서버의 디스크가 깨져도 클라이언트 repository 중에서 하나로부터 복구할 수 있다 (서버에만 적용했던 설정은 복구하지 못하지만 모든 데이터는 복구된다).

`git clone <url>` 명령으로 저장소를 clone한다. `libgit2` 라이브러리 소스코드를 clone하려면 아래과 같이 실행한다.
```
$ git clone https://github.com/libgit2/libgit2
```

이 명령은 “libgit2” 라는 디렉토리를 만들고 그 안에 `.git` 디렉토리를 만든다. 그리고 repository의 데이터를 모두 가져와서 자동으로 가장 최신 버전을 checkout한다. `libgit2` 디렉토리로 이동하면 checkout으로 생성한 파일을 볼 수 있으며, 하고자 하는 일을 즉시 시작할 수 있다.

아래과 같은 명령을 사용하여 저장소를 clone하면 `libgit2`이 아니라 다른 디렉토리 이름으로 clone할 수도 있다.
```
$ git clone https://github.com/libgit2/libgit2 mylibgit
```

디렉토리 이름이 `mylibgit`이라는 것만 빼면 이 명령의 결과와 앞선 명령의 결과는 같다.

Git은 다양한 프로토콜을 지원한다. 이제까지는 `https://` 프로토콜을 사용했지만 `git://`이나 `user@server:path/to/repo.git`처럼 SSH 프로토콜을 사용할 수도 있다. 자세한 내용은 [Getting Git on a Server](https://git-scm.com/book/en/v2/ch00/_getting_git_on_a_server)에서 찾아 볼 수 있으며, 각 프로토콜의 장단점과 Git repository에 접근하는 방법을 설명한다.

## 수정하고 Repository에 저장하기 <a id="recording-changes-to-repository"></a>
이제 우리는 로칼 컴퓨터에 Git repository를 하나 만들었고, 그 모든 파일을 checkout하여  디렉토리에 **working copy** 또한 만든다. 이제 파일을 수정하고 파일의 스냅샷을 커밋해 보자. 파일을 수정하다가 저장하고 싶으면 스냅샷을 커밋한다.

워킹 디렉토리의 모든 파일은 크게 **tracked(관리대상임)**와 **untracked(관리대상이 아님)**로 나눈다. tracked 파일은 이미 스냅샷에 포함돼 있던 파일이다. tracked 파일은 다시 unmodified(수정하지 않음)와 modified(수정함) 그리고 staged(커밋으로 저장소에 기록할) 상태 중 하나이다. tracked 파일은 간단히 말해서 Git이 알고 있는 파일이라는 것이다.

그리고 나머지 파일은 모두 untracked 파일이다. untracked 파일은 워킹 디렉토리에 있는 파일 중 스냅샷에도 staging area에도 포함되지 않은 파일이다. 처음 저장소를 clone 하면 모든 파일은 tracked이면서 unmodified 상태이다. 파일을 checkout한 다음 아무것도 수정하지 않았기 때문이다.

마지막 커밋 이후 아직 아무것도 수정하지 않은 상태에서 어떤 파일을 수정하면 Git은 그 파일을 modified 상태로 인식한다. 실제로 커밋을 하기 위해서는 이 수정한 파일을 staged 상태로 만들고, staged 상태의 파일을 커밋한다. 이런 라이프사이클은 계속 반복된다.

![lifecycle](../images/lifecycle.png)<br>
Figure 1-4. 파일의 라이프사이클<br>
<p style='text-align: right;'><sup>(출처: http://127.0.0.1:8000/gitngitflow/git/getting-started-git/)</sup></p>

### 파일의 상태 확인하기 <a id="checking-status-of-files"></a>
파일의 상태를 확인하려면 보통 `git status` 명령을 사용한다. clone 한 후에 바로 이 명령을 실행하면 아래과 같은 메시지를 볼 수 있다.
```
$ git status
On branch main
Your branch is up-to-date with 'origin/main'.
nothing to commit, working directory clean
```

> **Note**: 기본적으로 GitHub, GitLab은 소스 코드 저장소의 기본 버전에 "*master*"라는 용어를 사용한다. 개발자는 자신의 코드를 추가하는 컴퓨터에서 "*master*"의 복사본을 만든 다음 변경사항을 "*master*" 브랜치에 다시 병합한다. GitHub는 "2020년 10월 1일, 새로 생성하는 모든 repository는 *master* 대신 기본 브랜치로 *main*을 사용하게 된다."라고 말했다<a href="#foot-1" id="foot-1-ref"><sup>1</sup></a>. 따라서 여기서도 이후 *main*을 사용하도록 한다.

위의 내용은 파일을 하나도 수정하지 않았다는 것을 말해준다. 즉, tracked 파일이 하나도 수정되지 않았다는 의미이다. untracked 파일은 아직 없어서 목록에 나타나지 않는다. 그리고 현재 작업 중인 브랜치를 알려주며 서버의 같은 브랜치로부터 진행된 작업이 없는 것을 나타낸다. 기본 브랜치가 main이기 때문에 현재 브랜치 이름이 “main”으로 나타난다.  브랜치 관련 내용은 차차 알아가자. [Git 브랜치](git-branch.md)에서 브랜치에 대해 다룬다.

프로젝트에 README 파일을 만들어보자. README 파일은 새로 만든 파일이기 때문에 `git status`를 실행하면 'untracked files’에 들어 있다.
```bash
$ echo 'My Project' > README
$ git status
On branch main
Your branch is up-to-date with 'origin/main'.
Untracked files:
  (use "git add <file>..." to include in what will be committed)

    README

nothing added to commit but untracked files present (use "git add" to track)
```

`README` 파일은 “untracked files” 부분에 속해 있는데 이것은 README 파일이 untracked 상태라는 것을 말한다. Git은 untracked 파일을 아직 스냅샷(커밋)에 포함되지 파일이라고 본다. 파일이 tracked 상태가 되기 전까지는 Git은 절대 그 파일을 커밋하지 않는다. 그래서 개발하면서 생성되는 바이너리 파일 등을 커밋하는 실수는 하지 않게 된다. README 파일을 추가해서 직접 tracked 상태로 만들어 보자.

### 새 파일 Tracking <a id="tracking-new-files"></a>
`git add` 명령으로 새 파일의 tracking을 시작할 수 있다. 아래 명령을 실행하면 Git은 `README` 파일의 tracking을 시작한다.
```bash
$ git add README
```

`git status` 명령을 다시 실행하면 `README` 파일이 tracked 상태이면서 커밋에 추가될 staged 상태라는 것을 확인할 수 있다.
```bash
$ git status
On branch main
Your branch is up-to-date with 'origin/main'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    new file:   README
```

“Changes to be committed” 에 들어 있는 파일은 staged 상태라는 것을 의미한다. 커밋하면 `git add`를 실행한 시점의 파일이 커밋되어 repository 히스토리에 남는다. 앞에서 `git init` 명령을 실행한 후, `git add (files)` 명령을 실행했던 걸 기억할 것이다. 이 명령을 통해 디렉토리에 있는 파일을 추적하고 관리할 수 있다. `git add` 명령은 파일 또는 디렉토리의 경로를 아규먼트로 받는다. 디렉토리면 하위에 있는 모든 파일들까지 재귀적(recursive)으로 추가한다.

### 수정된 파일의 Staging <a id="staging-modified-files"></a>
이미 tracked 상태인 파일을 수정하는 법을 알아보자. `CONTRIBUTING.md`라는 파일을 수정하고 나서 `git status` 명령을 다시 실행하면 결과는 아래와 같다.
```bash
$ git status
On branch main
Your branch is up-to-date with 'origin/main'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    new file:   README

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

    modified:   CONTRIBUTING.md
```

이 `CONTRIBUTING.md` 파일은 “Changes not staged for commit” 상태에 있다. 이것은 수정한 파일이 tracked 상태이지만 아직 staged 상태는 아니라는 것이다. staged 상태로 만들려면 `git add` 명령을 실행해야 한다. `git add` 명령은 파일을 새로 추적할 때도 사용하고 수정한 파일을 staged 상태로 만들 때도 사용한다. merge할 때 충돌난 상태의 파일을 resolve 상태로 만들때도 사용한다. add의 의미는 프로젝트에 파일을 추가한다기 보다는 다음 커밋에 추가한다고 이해하는 것이 바람직하다. `git add` 명령을 실행하여 `CONTRIBUTING.md` 파일을 staged 상태로 만들고 `git status` 명령으로 결과를 확인해 보자.
```bash
$ git add CONTRIBUTING.md
$ git status
On branch main
Your branch is up-to-date with 'origin/main'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    new file:   README
    modified:   CONTRIBUTING.md
```

두 파일 모두 staged 상태이므로 다음 커밋에 포함된다. 하지만 아직 더 수정해야 한다는 것을 알게 되어 바로 커밋하지 못하는 상황이 되었다고 가장해 보자. 이 상황에서 `CONTRIBUTING.md` 파일을 열고 수정한다. 이제 커밋할 준비가 다 됐다고 생각할 수 있지만, Git은 그렇지 않다. `git status` 명령으로 파일의 상태를 다시 확인해 보자.
```bash
$ vim CONTRIBUTING.md
$ git status
On branch main
Your branch is up-to-date with 'origin/main'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    new file:   README
    modified:   CONTRIBUTING.md

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

    modified:   CONTRIBUTING.md
```

헐! `CONTRIBUTING.md`가 staged 상태이면서 동시에 unstaged 상태로 나온다. 어떻게 이런 일이 가능할까? `git add` 명령을 실행하면 Git은 파일을 바로 staged 상태로 만든다. 지금 이 시점에서 커밋을 하면 `git commit` 명령을 실행하는 시점의 버전이 커밋되는 것이 아니라 마지막으로 `git add` 명령을 실행했을 때의 버전이 커밋된다. 그러므로 `git add` 명령을 실행한 후에 또 파일을 수정하면 `git add` 명령을 다시 실행해서 최신 버전을 staged 상태로 만들어야 한다.

```bash
$ git add CONTRIBUTING.md
$ git status
On branch main
Your branch is up-to-date with 'origin/main'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    new file:   README
    modified:   CONTRIBUTING.md
```

### 파일 상태를 짤막하게 확인 <a id="short-status"></a>
`git status` 명령으로 확인할 수 있는 내용이 좀 많아 보일 수 있다. 사실 그렇다. 좀 더 간단하게 변경 내용을 보여주는 옵션이 있다. `git status -s` 또는 `git status --short` 처럼 옵션을 주면 현재 변경한 상태를 간단히 보여준다.
```bash
$ git status -s
 M README
MM Rakefile
A  lib/git.rb
M  lib/simplegit.rb
?? LICENSE.txt
```

아직 tracking하지 않는 새 파일 앞에 `??` 표시가 붙는다. staged 상태로 추가한 파일 중 새로 생성한 파일 앞에는 `A` 표시가, 수정한 파일 앞에는 `M` 표시가 붙는다. 위 명령의 결과에서 상태정보 컬럼에는 두 가지 정보를 보여준다. 왼쪽에는 staging area에서의 상태를, 오른쪽에는 working tree에서의 상태를 표시한다. `README` 파일 같은 경우 내용을 변경했지만 아직 staged 상태로 추가하지는 않았다. `lib/simplegit.rb` 파일은 내용을 변경하고 staged 상태로 추가까지 한 상태이다. 위 결과에서 차이점을 비교해보자. `Rakefile`은 변경하고 staged 상태로 추가한 후 또 내용을 변경해서 staged이면서 unstaged 상태이다.

### 파일 무시 <a id="ignoring-files"></a>
Git이 관리할 필요가 없는 파일이 있을 수 있다. 보통 로그 파일이나 빌드 시스템이 자동으로 생성한 파일이 그렇다. 그런 파일을 무시하려면 `.gitignore` 파일을 만들고 그 안에 무시할 파일 패턴을 적는다. 아래는 `.gitignore` 파일의 예이다.
```bash
$ cat .gitignore
*.[oa]
*~
```

첫번째 라인은 확장자가 “.o” 나 “.a” 인 파일을 Git이 무시하라는 것이고, 둘째 라인은 `~` 로 끝나는 모든 파일을 무시하라는 것이다. 보통 대부분의 텍스트 편집기에서 임시파일로 사용하는 파일 이름이기 때문이다. “.o” 와 “.a” 는 각각 빌드 시스템이 만들어내는 객체와 아카이브 파일이고 `~` 로 끝나는 파일은 Emacs나 VI 같은 텍스트 편집기가 임시로 만들어내는 파일이다. 또 log, tmp, pid 같은 디렉토리나, 자동으로 생성하는 문서같은 것들도 추가할 수 있다. `.gitignore` 파일은 보통 처음에 만들어 두는 것이 편리하다. 그래서 Git repository에 커밋하고 싶지 않은 파일을 실수로 커밋하는 일을 방지할 수 있다.

`.gitignore` 파일에 입력하는 패턴은 아래 규칙을 따른다.

- 아무것도 없는 라인이나, `#`로 시작하는 라인은 무시한다.
- 표준 Glob 패턴을 사용한다. 이는 프로젝트 전체에 적용된다.
- 슬래시(/)로 시작하면 하위 디렉토리에 적용되지(recursivity) 않는다.
- 디렉토리는 슬래시(/)를 끝에 사용하는 것으로 표현한다.
- 느낌표(!)로 시작하는 패턴의 파일은 무시하지 않는다.

Glob 패턴은 단순한 정규표현식으로 생각하면 되고 보통 쉘에서 많이 사용한다. 애스터리스크(`*`)는 문자가 하나도 없거나 하나 이상을 의미하고, `[abc]` 는 중괄호 안에 있는 문자 중 하나를 의미한다 (따라서 이 경우에는 a, b, c). 물음표(`?`)는 문자 하나를 말하고, `[0-9]` 처럼 중괄호 안의 캐릭터 사이에 하이픈(`-`)을 사용하면 그 캐릭터 사이에 있는 문자 하나를 말한다. 애스터리스크 2개를 사용하여 디렉토리 안의 디렉토리 까지 지정할 수 있다. `a/**/z` 패턴은 `a/z`, `a/b/z`, `a/b/c/z` 디렉토리에 사용할 수 있다.

다음은 `.gitignore` 파일의 예이다.
```bash
# ignore all .a files
*.a

# but do track lib.a, even though you're ignoring .a files above
!lib.a

# only ignore the TODO file in the current directory, not subdir/TODO
/TODO

# ignore all files in any directory named build
build/

# ignore doc/notes.txt, but not doc/server/arch.txt
doc/*.txt

# ignore all .pdf files in the doc/ directory and any of its subdirectories
doc/**/*.pdf
```

**Hint**: GitHub에서는 다양한 프로젝트에서 자주 사용하는 `.gitignore` 예제를 관리하고 있다. 어떤 내용을 넣어야 할지 막막하다면 [https://github.com/github/gitignore](https://github.com/github/gitignore) 사이트에서 적당한 예제를 찾을 수 있다.

**Note**: `.gitignore`를 사용하는 간단한 방식은 하나의 `.gitignore` 파일을 최상위 디렉토리에 하나 두고 모든 하위 디렉토리에까지 적용시키는 방식이다. 물론 `.gitignore` 파일을 하나만 두는 것이 아니라 하위 디렉토리에도 추가할 수도 있다. 이처럼 하위 디렉토리에 정릐된 `.gitignore` 파일은 현재 `.gitignore` 파일이 위치한 디렉토리에 적용된다. 리눅스 커널 소스 repository에는 206 `.gitignore` 파일이 있다. 다수의 `.gitignore` 파일을 두고 정책을 적용하는 부분은 이 페이지에서 다루지 않는다.

### Staged와 Unstaged 상태의 변경 내용 보기 <a id="viewing-staged-and-unstaged-changes"></a>
단순히 파일이 변경됐다는 사실이 아니라 어떤 내용이 변경됐는지 살펴보려면 `git status` 명령이 아니라 `git diff` 명령을 사용해야 한다. 보통 우리는 '수정했지만, 아직 staged 파일이 아닌 것?'과 '어떤 파일이 staged 상태인지?'가 궁금하기 때문에 `git status` 명령으로도 충분하다. 더 자세히 보려면 `git diff` 명령을 사용하는데 patch처럼 어떤 행을 추가했고 삭제했는 지 궁금할 때 사용한다. `git diff`는 나중에 더 자세히 다룬다.

`README` 파일을 수정해서 `staged` 상태로 만들고 `CONTRIBUTING.md` 파일은 그냥 수정만 해둔다. 이 상태에서 `git status` 명령을 실행하면 아래와 같은 메시지를 볼 수 있다.
```bash
$ git status
On branch main
Your branch is up-to-date with 'origin/main'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    modified:   README

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

    modified:   CONTRIBUTING.md
```

`git diff` 명령을 실행하면 수정했지만 아직 staged 상태가 아닌 파일을 비교해 볼 수 있다.
```bash
$ git diff
diff --git a/CONTRIBUTING.md b/CONTRIBUTING.md
index 8ebb991..643e24f 100644
--- a/CONTRIBUTING.md
+++ b/CONTRIBUTING.md
@@ -65,7 +65,8 @@ branch directly, things can get messy.
 Please include a nice description of your changes when you submit your PR;
 if we have to read the whole diff to figure out why you're contributing
 in the first place, you're less likely to get feedback and have your change
-merged in.
+merged in. Also, split your changes into comprehensive chunks if your patch is
+longer than a dozen lines.

 If you are starting to work on a particular area, feel free to submit a PR
 that highlights your work in progress (and note in the PR title that it's
```

이 명령은 워킹 디렉토리에 있는 것과 staging area에 있는 것을 비교한다. 그래서 수정하고 아직 stage 하지 않은 것을 보여준다.

만약 커밋하려고 staging area에 넣은 파일의 변경 부분을 보고 싶으면 `git diff --staged` 옵션을 사용한다. 이 명령은 repository에 커밋한 것과 staging area에 있는 것을 비교한다.
```bash
$ git diff --staged
diff --git a/README b/README
new file mode 100644
index 0000000..03902a1
--- /dev/null
+++ b/README
@@ -0,0 +1 @@
+My Project
```

주의하여야 할 점은 `git diff` 명령은 마지막으로 커밋한 후에 수정한 것들 전부를 보여주지 않는다. `git diff`는 unstaged 상태인 것들만 보여준다. 수정한 파일을 모두 staging area에 넣었다면 `git diff` 명령은 아무것도 출력하지 않는다.

`CONTRIBUTING.md` 파일을 stage한 후 다시 수정해도  `git diff` 명령을 사용할 수 있다. 이때는 staged 상태인 것과 unstaged 상태인 것을 비교한다.
```bash
$ git add CONTRIBUTING.md
$ echo '# test line' >> CONTRIBUTING.md
$ git status
On branch main
Your branch is up-to-date with 'origin/main'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    modified:   CONTRIBUTING.md

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

    modified:   CONTRIBUTING.md
```

`git diff` 명령으로 unstaged 상태인 변경 부분을 확인할 수 있다.

```bash
$ git diff
diff --git a/CONTRIBUTING.md b/CONTRIBUTING.md
index 643e24f..87f08c8 100644
--- a/CONTRIBUTING.md
+++ b/CONTRIBUTING.md
@@ -119,3 +119,4 @@ at the
 ## Starter Projects

 See our [projects list](https://github.com/libgit2/libgit2/blob/development/PROJECTS.md).
+# test line
```

staged 상태인 파일은 `git diff --cached` 옵션으로 확인한다. `--staged` 옵션과 `--cached` 옵션은 같은 의미이다.

```bash
$ git diff --cached
diff --git a/CONTRIBUTING.md b/CONTRIBUTING.md
index 8ebb991..643e24f 100644
--- a/CONTRIBUTING.md
+++ b/CONTRIBUTING.md
@@ -65,7 +65,8 @@ branch directly, things can get messy.
 Please include a nice description of your changes when you submit your PR;
 if we have to read the whole diff to figure out why you're contributing
 in the first place, you're less likely to get feedback and have your change
-merged in.
+merged in. Also, split your changes into comprehensive chunks if your patch is
+longer than a dozen lines.

 If you are starting to work on a particular area, feel free to submit a PR
 that highlights your work in progress (and note in the PR title that it's
```

**Note**: **외부 도구를 이용하여 비교하기**
이 페이지에서는 계속 `git diff` 명령을 이곳 저곳에서 사용한다. 즐겨 쓰거나 결과를 아름답게 보여주는 diff 도구가 있으면 사용할 수 있다. `git diff` 대신 `git difftool` 명령을 사용해서 emerge 및 vimdiff와 같은 도구로 비교할 수 있다. 상용 제품도 사용할 수 있다. `git difftool --tool-help`라는 명령은 사용 가능한 도구를 보여준다.

### 변경사항 커밋 <a id="committing-changes"></a>
수정한 것을 커밋하기 위해 staging area에 파일을 정리했다. unstaged 상태의 파일은 커밋되지 않는다는 것을 기억하자. Git은 생성하거나 수정하고 나서 `git add` 명령으로 추가하지 않은 파일은 커밋되지 않는다. 그 파일은 여전히 modified 상태로 남아 있다. 커밋하기 전에 `git status` 명령으로 모든 것이 staged 상태인지 확인할 수 있다. 그 후에 `git commit`을 실행하여 커밋한다.

```bash
$ git commit
```

Git 설정에 지정된 편집기가 실행되고, 아래와 같은 텍스트가 자동으로 포함된다 (아래 예제는 Vim 편집기의 화면이다. 이 편집기는 쉘의 `EDITOR` 환경 변수에 등록된 편집기이다. 또 [Git 최초 설정](#first-time-git-setup) 에서 설명하였지만 `git config --global core.editor` 명령으로 어떤 편집기를 사용할지 설정할 수 있다).

편집기는 아래와 같은 내용을 표시한다(아래 예제는 Vim 편집기).
```
# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
# On branch main
# Your branch is up-to-date with 'origin/main'.
#
# Changes to be committed:
#	new file:   README
#	modified:   CONTRIBUTING.md
#
~
~
~
".git/COMMIT_EDITMSG" 9L, 283C
```

자동으로 생성되는 커밋 메시지의 첫 라인은 비어 있고 둘째 라인부터 `git status` 명령의 결과가 채워진다. 커밋한 내용을 쉽게 기억할 수 있도록 이 메시지를 포함할 수도 있고 메시지를 전부 지우고 새로 작성할 수 있다 (정확히 뭘 수정했는지도 보여줄 수 있는데, `git commit` 에 `-v` 옵션을 추가하면 편집기에 diff 메시지도 추가된다). 내용을 저장하고 편집기를 종료하면 Git은 입력된 내용(#로 시작하는 내용을 제외한)으로 새 커밋을 하나 완성한다.

메시지를 인라인으로 첨부할 수도 있다. `commit` 명령을 실행할 때 아래와 같이 `-m` 옵션을 사용한다.
```bash
$ git commit -m "Story 182: Fix benchmarks for speed"
[main 463dc4f] Story 182: Fix benchmarks for speed
 2 files changed, 2 insertions(+)
 create mode 100644 README
```

이렇게 첫번째 커밋을 작성해보았다. `commit` 명령은 몇 가지 정보를 출력한다. 위 예제에서 (`main`) 브랜치에 커밋했고 체크섬은 (`463dc4f`)이라고 알려준다. 그리고 수정한 파일이 몇 개이고 삭제됐거나 추가된 라인이 몇 라인인지 알려준다.

Git은 staging area에 속한 스냅샷을 커밋한다는 것을 기억해야 한다. 수정은 했지만, 아직 staging area에 넣지 않은 것은 다음에 커밋할 수 있다. 커밋할 때마다 프로젝트의 스냅샷을 기록하기 때문에 나중에 스냅샷끼리 비교하거나 예전 스냅샷으로 되돌릴 수 있다.

### Staging Area 생략 <a id="skipping-staging-area"></a>
staging area는 커밋할 파일을 정리한다는 점에서 매우 유용하지만 복잡하기만 하고 필요하지 않은 때도 있다. 쉽게 staging area를 생략할 수 있다. `git commit` 명령을 실행할 때 `-a` 옵션을 추가하면 Git은 tracked 상태의 파일을 자동으로 staging area에 넣는다. 그래서 `git add` 명령을 실행하는 수고를 덜 수 있다.
```bash
$ git status
On branch main
Your branch is up-to-date with 'origin/main'.
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

    modified:   CONTRIBUTING.md

no changes added to commit (use "git add" and/or "git commit -a")
$ git commit -a -m 'added new benchmarks'
[main 83e38c7] added new benchmarks
 1 file changed, 5 insertions(+), 0 deletions(-)
```

위의 예제에서는 커밋하기 전에 `git add` 명령으로 `CONTRIBUTING.md` 파일을 추가하지 않았다는 점을 눈여겨보자. `-a` 옵션을 사용하면 모든 파일이 자동으로 추가된다. 편리한 옵션이긴 하지만 주의 깊게 사용해야 한다. 생각 없이 이 옵션을 사용하다 보면 추가하지 말아야 할 변경사항도 추가될 수 있기 때문이다.

### 파일 삭제 <id a="removing-files"></a>
Git에서 파일을 제거하려면 `git rm` 명령으로 tracked 상태의 파일을 삭제한 후에 (정확하게는 staging area에서 삭제하는 것) 커밋해야 한다. 이 명령은 워킹 디렉토리에 있는 파일도 삭제하기 때문에 실제로 파일도 지워진다.

Git 명령을 사용하지 않고 단순히 워킹 디렉터리에서 파일을 삭제하고 `git status` 명령으로 상태를 확인하면 Git은 현재 “Changes not staged for commit” (즉, **unstaged** 상태)라고 표시해준다.
```bash
$ rm PROJECTS.md
$ git status
On branch main
Your branch is up-to-date with 'origin/main'.
Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        deleted:    PROJECTS.md

no changes added to commit (use "git add" and/or "git commit -a")
```

그리고 `git rm` 명령을 실행하면 삭제한 파일은 staged 상태가 된다.
```bash
$ git rm PROJECTS.md
rm 'PROJECTS.md'
$ git status
On branch master
Your branch is up-to-date with 'origin/main'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    deleted:    PROJECTS.md
```

커밋하면 파일은 삭제되고 Git은 이 파일을 더는 추적하지 않는다. 이미 파일을 수정했거나 staging area에 추가했다면 `-f` 옵션을 주어 강제로 삭제해야 한다. 이 점은 실수로 데이터를 삭제하지 못하도록 하는 안전장치이다. 커밋하지 않고 수정한 데이터는 Git으로 복구할 수 없기 때문이다.

또 staging area에서만 제거하고 워킹 디렉토리에 있는 파일은 지우지 않고 남겨둘 수 있다. 다시 말해서 하드디스크에 있는 파일은 그대로 두고 Git만 추적하지 않게 한다. 이것은 `.gitignore` 파일에 추가하는 것을 빼먹었거나 대용량 로그 파일이나 컴파일된 파일인 `.a` 파일 같은 것을 실수로 추가했을 때 쓴다. `--cached` 옵션을 사용하여 명령을 실행한다.
```bash
$ git rm --cached README
```

여러 개의 파일이나 디렉토리를 한꺼번에 삭제할 수도 있다. 아래와 같이 `git rm` 명령에 file-glob 패턴을 사용한다.
```bash
$ git rm log/\*.log
```

`*` 앞에 `\`를 사용한 것을 기억하자. 파일명 확장 기능은 쉘에만 있는 것이 아니라 Git 자체에도 있기 때문에 필요하다. 이 명령은 `log/` 디렉토리에 있는 `.log` 파일을 모두 삭제한다. 아래의 예제처럼 할 수도 있다.
```bash
$ git rm \*~
```

이 명령은 `~`로 끝나는 파일을 모두 삭제한다.

### 파일 이름 변경 <a id="moving-files"></a>
Git은 다른 VCS 시스템과는 달리 파일 이름의 변경이나 파일의 이동을 명시적으로 관리하지 않는다. 다시 말해서 파일 이름이 변경됐다는 별도의 정보를 저장하지 않는다. Git은 똑똑해서 굳이 파일 이름이 변경되었다는 것을 추적하지 않아도 아는 방법이 있다. 파일의 이름이 변경된 것을 Git이 어떻게 알아내는지 살펴보자.

이렇다면 Git에 `mv` 명령이 있는 게 좀 이상하겠지만, 아래와 같이 파일 이름을 변경할 수 있다.
```bash
$ git mv [file_from] [file_to]
```

잘 동작한다. 이 명령을 실행하고 Git의 상태를 확인해보면 Git은 이름이 바뀐 사실을 알고 있다.

```bash
$ git mv README.md README
$ git status
On branch main
Your branch is up-to-date with 'origin/main'.
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    renamed:    README.md -> README
```

사실 `git mv` 명령은 아래 명령어를 수행한 것과 완전 같다.
```
$ mv README.md README
$ git rm README.md
$ git add README
```

`git mv` 명령은 일종의 단축 명령어이다. 이 명령으로 파일 이름을 변경하여도 되고 `mv` 명령으로 파일 이름을 직접 바꿔도 된다. 단지 `git mv` 명령은 편리하게 세 명령을 실행해주는 것 뿐이다. 어떤 도구로 이름을 바꿔도 상관없다. 중요한 것은 이름을 변경하고 나서 꼭 `rm`/`add` 명령을 실행해야 한다는 것 뿐이다.

## 커밋 히스토리 조회하기 <a id="viewing-commit-history"></a>
새로 repository를 만들어서 몇 번 커밋을 했을 수도 있고, 커밋 히스토리가 있는 repository를 clone했을 수도 있다. 어쨌든 가끔 repository의 히스토리를 보고 싶을 때가 있다. Git에는 히스토리를 조회하는 명령어인 `git log`가 있다.

이 예제에서는 “simplegit” 이라는 매우 단순한 프로젝트를 사용한다. 아래와 같이 이 프로젝트를 clone 한다.
```
$ git clone https://github.com/schacon/simplegit-progit
```

이 프로젝트 디렉토리에서 `git log` 명령을 실행하면 아래와 같이 출력된다.
```
commit ca82a6dff817ec66f44342007202690a93763949 (HEAD -> master, origin/master, origin/HEAD)
Author: Scott Chacon <schacon@gmail.com>
Date:   Mon Mar 17 21:52:11 2008 -0700

    changed the verison number

commit 085bb3bcb608e1e8451d4b2432f8ecbe6306e7e7
Author: Scott Chacon <schacon@gmail.com>
Date:   Sat Mar 15 16:40:33 2008 -0700

    removed unnecessary test code

commit a11bef06a3f659402fe7563abf99ad00de2209e6
Author: Scott Chacon <schacon@gmail.com>
Date:   Sat Mar 15 10:31:28 2008 -0700

    first commit
```
특별한 인지 없이 `git log` 명령을 실행하면 repository의 커밋 히스토리를 시간순으로 보여준다. 즉, 가장 최근의 커밋이 가장 먼저 나온다. 그리고 이어서 각 커밋의 SHA-1 체크섬, 저자 이름, 저자 이메일, 커밋한 날짜, 커밋 메시지를 보여준다.

원하는 히스토리를 검색할 수 있도록 `git log` 명령은 매우 다양한 옵션을 지원한다. 여기에서는 자주 사용하는 옵션을 설명한다.

여러 옵션 중 `-p`, `--patch`는 굉장히 유용한 옵션이다. `-p`는 각 커밋의 diff 결과를 보여준다. 다른 유용한 옵션으로 `-2`가 있는데 최근 두 개의 결과만 보여주는 옵션이다.
```bash
$ git log -p -2
commit ca82a6dff817ec66f44342007202690a93763949 (HEAD -> master, origin/master, origin/HEAD)
Author: Scott Chacon <schacon@gmail.com>
Date:   Mon Mar 17 21:52:11 2008 -0700

    changed the verison number

diff --git a/Rakefile b/Rakefile
index a874b73..8f94139 100644
--- a/Rakefile
+++ b/Rakefile
@@ -5,7 +5,7 @@ require 'rake/gempackagetask'
 spec = Gem::Specification.new do |s|
     s.platform  =   Gem::Platform::RUBY
     s.name      =   "simplegit"
-    s.version   =   "0.1.0"
+    s.version   =   "0.1.1"
     s.author    =   "Scott Chacon"
     s.email     =   "schacon@gmail.com"
     s.summary   =   "A simple gem for using Git in Ruby code."

commit 085bb3bcb608e1e8451d4b2432f8ecbe6306e7e7
Author: Scott Chacon <schacon@gmail.com>
Date:   Sat Mar 15 16:40:33 2008 -0700

    removed unnecessary test code

diff --git a/lib/simplegit.rb b/lib/simplegit.rb
index a0a60ae..47c6340 100644
--- a/lib/simplegit.rb
+++ b/lib/simplegit.rb
@@ -18,8 +18,3 @@ class SimpleGit
     end

 end
-
-if $0 == __FILE__
-  git = SimpleGit.new
-  puts git.show
-end
```

이 옵션은 직접 diff를 실행한 것과 같은 결과를 출력하기 때문에 동료가 무엇을 커밋했는지 리뷰하고 빨리 조회하는데 유용하다. 또 `git log` 명령에는 히스토리의 통계를 보여주는 옵션도 있다. `--stat` 옵션으로 각 커밋의 통계 정보를 조회할 수 있다.
```bash
$ git log --stat
commit ca82a6dff817ec66f44342007202690a93763949 (HEAD -> master, origin/master, origin/HEAD)
Author: Scott Chacon <schacon@gmail.com>
Date:   Mon Mar 17 21:52:11 2008 -0700

    changed the verison number

 Rakefile | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

commit 085bb3bcb608e1e8451d4b2432f8ecbe6306e7e7
Author: Scott Chacon <schacon@gmail.com>
Date:   Sat Mar 15 16:40:33 2008 -0700

    removed unnecessary test code

 lib/simplegit.rb | 5 -----
 1 file changed, 5 deletions(-)

commit a11bef06a3f659402fe7563abf99ad00de2209e6
Author: Scott Chacon <schacon@gmail.com>
Date:   Sat Mar 15 10:31:28 2008 -0700

    first commit

 README           |  6 ++++++
 Rakefile         | 23 +++++++++++++++++++++++
 lib/simplegit.rb | 25 +++++++++++++++++++++++++
 3 files changed, 54 insertions(+)
```

이 결과에서 `--stat` 옵션은 어떤 파일이 수정됐는지, 얼마나 많은 파일이 변경됐는지, 또 얼마나 많은 행을 추가하거나 삭제했는지 보여준다. 요약 정보는 가장 뒤쪽에 보여준다.

다른 또 유용한 옵션은 `--pretty` 옵션이다. 이 옵션을 통해 히스토리 내용을 보여줄 때 기본 형식 이외에 여러 가지 중에 하나를 선택할 수 있다. 몇개 선택할 수 있는 옵션 값이 있다. `oneline` 옵션은 각 커밋을 한 행으로 보여준다. 이 옵션은 많은 커밋을 한 번에 조회할 때 유용하다. 추가로 `short`, `full`, `fuller` 옵션도 있는데 이것은 정보를 조금씩 가감해서 보여준다.
```bash
$ git log --pretty=oneline
ca82a6dff817ec66f44342007202690a93763949 (HEAD -> master, origin/master, origin/HEAD) changed the verison number
085bb3bcb608e1e8451d4b2432f8ecbe6306e7e7 removed unnecessary test code
a11bef06a3f659402fe7563abf99ad00de2209e6 first commit
```

가장 흥미로운 옵션은 `format` 옵션이다. 나만의 포맷으로 결과를 출력하고 싶을 때 사용한다. 특히 결과를 다른 프로그램으로 파싱하고자 할 때 유용하다. 이 옵션을 사용하면 포맷과 정확하게 일치시킬 수 있기 때문에 Git을 새 버전으로 바꿔도 결과 포맷이 바뀌지 않는다.
```bash
$ git log --pretty=format:"%h - %an, %ar : %s"
ca82a6d - Scott Chacon, 14년 전 : changed the verison number
085bb3b - Scott Chacon, 14년 전 : removed unnecessary test code
a11bef0 - Scott Chacon, 14년 전 : first commit
```

`git log --pretty=format`에서 사용할 수 있는 [유용한 옵션](https://git-scm.com/book/ko/v2/ch00/pretty_format)은  아래와 같다

F. `git log --pretty=format`의 유용한 옵션

| 옵션 | 설명 |
|:---:|:---|
| `%H` | 커밋 해시|
| `%h` | 짧은 길이 커밋 해시 |
| `%T` | 트리 해시 |
| `%t` | 짧은 길이 트리 해시 |
| `%P` | 부모 해시 | 
| `%p` | 짧은 길이 부모 해시 |
| `%an` | 저자 이름 |
| `%ae` | 저자 메일 |
| `%ad` | 저자 시각 (형식은 –-date=옵션 참고) |
| `%ar` | 저자 상대적 시각 |
| `%cn` | 커미터 이름 |
| `%ce` | 커미터 메일 |
| `%cd` | 커미터 시각 |
| `%cr` | 커미터 상대적 시각 |
| `%s` | 요약 |

**저자(Author)**와 **커미터(Committer)**를 구분하는 것이 조금 이상해 보일 수 있다. 저자는 원래 작업을 수행한 원작자이고 커밋터는 마지막으로 이 작업을 적용한(저장소에 포함시킨) 사람이다. 만약 당신이 어떤 프로젝트에 패치를 보냈고 그 프로젝트의 담당자가 패치를 적용했다면 두 명의 정보를 모두 알 필요가 있다. 그래서 이 경우 당신이 저자고 그 담당자가 커미터다. [분산 환경에서의 Git](https://git-scm.com/book/ko/v2/ch00/ch05-distributed-git) 에서 이 주제에 대해 자세히 설명하고 있다.

`oneline` 옵션과 `format` 옵션은 `--graph` 옵션과 함께 사용할 때 더 유용하다. 이 명령은 브랜치와 merge 히스토리를 보여주는 ASCII 그래프를 출력한다.
```bash
$ git log --pretty=format:"%h %s" --graph
* 2d3acf9 Ignore errors from SIGCHLD on trap
*  5e3ee11 Merge branch 'master' of git://github.com/dustin/grit
|\
| * 420eac9 Add method for getting the current branch
* | 30e367c Timeout code and tests
* | 5a09431 Add timeout protection to grit
* | e1193f8 Support for heads with slashes in them
|/
* d6016bc Require time for xmlschema
*  11d191e Merge branch 'defunkt' into local
```

앞으로 살펴볼 브랜치나 merge 결과의 히스토리를 위와 같이 출력하면 훨씬 쉽게 읽을 수 있다.

`git log` 명령의 기본적인 옵션과 출력물의 형식에 관련된 옵션을 살펴보았다. `git log` 명령은 앞서 살펴본 것보다 더 많은 옵션을 지원한다. `git log` 주요 옵션 는 지금 설명한 것과 함께 유용하게 사용할 수 있는 옵션이다. 각 옵션으로 어떻게 log 명령 결과를 출력할 수 있는지 보여준다.

표 2. `git log` 주요 옵션

| 옵션 | 설명 |
|:---|:---|
| `-p` | 각 커밋에 적용된 패치를 보여준다. |
| `--stat` | 각 커밋에서 수정된 파일의 통계정보를 보여준다. |
| `--shortstat` | `--stat` 명령의 결과 중에서 수정한 파일, 추가된 라인, 삭제된 라인만 보여준다. |
| `--name-only` | 커밋 정보중에서 수정된 파일의 목록만 보여준다. |
| `--name-status` | 수정된 파일의 목록을 보여줄 뿐만 아니라 파일을 추가한 것인지, 수정한 것인지, 삭제한 것인지도 보여준다. |
| `--abbrev-commit` | 40자 짜리 SHA-1 체크섬을 전부 보여주는 것이 아니라 처음 몇 자만 보여준다. |
| `--relative-date` | 정확한 시간을 보여주는 것이 아니라 “2 weeks ago” 처럼 상대적인 형식으로 보여준다. |
| `--graph` | 브랜치와 머지 히스토리 정보까지 ASCII 그래프로 보여준다. |
| `--pretty` |지정한 형식으로 보여준다. 이 옵션에는 oneline, short, full, fuller, format이 있다. format은 원하는 형식으로 출력하고자 할 때 사용한다. |
| `--oneline` | `--pretty=oneline`와 `--abbrev-commit` 두 옵션을 함께 사용한 것과 같다. |

### 조회 제한조건 <a id="limiting-log-output"></a>
출력 형식과 관련된 옵션을 살펴봤지만 `git log` 명령은 조회 범위를 제한하는 옵션들도 있다. 히스토리 전부가 아니라 부분만 조회한다. 이미 최근 두 개만 조회하는 `-2` 옵션을 살펴보았다. 실제 사용법은 '-<n>'이고 n은 최근 n개의 커밋을 의미한다. 사실 이 옵션을 자주 사용하지 않는다. Git은 기본적으로 출력을 pager류의 프로그램을 거쳐서 출력하므로 한 번에 한 페이지씩 보여준다.

반면 `--since`나 `--until` 같은 시간을 기준으로 조회하는 옵션은 매우 유용하다. 지난 2주 동안 만들어진 커밋들만 조회하는 명령은 아래와 같다.
```bash
$ git log --since=2.weeks
```

이 옵션은 다양한 형식을 지원한다."2008-01-15" 같이 정확한 날짜도 사용할 수 있고 "2 years 1 day 3 minutes ago" 같이 상대적인 기간을 사용할 수도 있다.

또 다른 기준도 있다. `--author` 옵션으로 저자를 지정하여 검색할 수도 있고 `--grep` 옵션으로 커밋 메시지에서 키워드를 검색할 수도 있다.

**Note**: `--author`와 `--grep` 옵션을 함께 사용하여 모두 만족하는 커밋을 찾으려면 `--all-match` 옵션도 반드시 함께 사용해야 한다.

매루 유용한 옵션으로 `-S`는 코드에서 추가되거나 제거된 내용 중에 특정 텍스트가 포함되어 있는 지를 검색한다. 예를 들어 어떤 함수가 추가되거나 제거된 커밋만을 찾아보려면 아래와 같은 명령을 사용한다.
```bash
$ git log -S function_name
```
마지막으로 파일 경로로 검색하는 옵션이 있는데 이것도 정말 유용하다. 디렉토리나 파일 이름을 사용하여 그 파일이 변경된 log의 결과를 검색할 수 있다. 이 옵션은 `--` 와 함께 경로 이름을 사용하는데 명령어 끝 부분에 쓴다  
```bash
git log -- path/to/file
```

`git log` 조회 범위를 제한하는 주요 옵션은 표3과 같다.

표 3. `git log` 조회 범위를 제한하는 옵션

| 옵션 | 설명 |
|:---|:---|
| `-(n)` | 최근 n 개의 커밋만 조회한다. |
| `--since`, `--after` | 명시한 날짜 이후의 커밋만 검색한다. |
| `--until`, `--before` | 명시한 날짜 이전의 커밋만 조회한다. |
| `--author` | 입력한 저자의 커밋만 보여준다. |
| `--committer` | 입력한 커미터의 커밋만 보여준다. |
| `--grep` | 커밋 메시지 안의 텍스트를 검색한다. |
| `-S` | 커밋 변경(추가/삭제) 내용 안의 텍스트를 검색한다. |

마지막으로 살펴볼 예제는 merge 커밋을 제외한 순수한 커밋을 확인해보는 명령이다. Junio Hamano가 2008년 10월에 Git 소스코드 repository에서 테스트 파일을 수정한 커밋들이다.
```bash
$ git log --pretty="%h - %s" --author=gitster --since="2008-10-01" \
   --before="2008-11-01" --no-merges -- t/
5610e3b - Fix testcase failure when extended attributes are in use
acd3b9e - Enhance hold_lock_file_for_{update,append}() API
f563754 - demonstrate breakage of detached checkout with symbolic link HEAD
d1a43f2 - reset --hard/read-tree --reset -u: remove unmerged new paths
51a94af - Fix "checkout --track -b newbranch" on detached HEAD
b0ad11e - pull: allow "git pull origin $something:$current_branch" into an unborn branch
```

총 4만여 개의 커밋 히스토리에서 이 명령의 검색 조건에 만족하는 것은 단 6개였다.

**Hint**: **머지 커밋 표시하지 않기**<br>
repository를 사용하는 워크플로우에 따라 merge 커밋이 차지하는 비중이 클 수도 있다. `--no-merges` 옵션을 사용하면 검색 결과에서 merge 커밋을 표시하지 않도록 할 수 있다.


## 되돌리기 <a id="undoing-things"></a>
개발을 하다보면 모든 단계에서 어떤 되돌리고(undo) 싶을 때가 있다. 이번에는 우리가 한 일을 되돌리는 방법을 살펴본다. 한 번 되돌리면 복구할 수 없기에 주의해야 한다. Git을 사용하면 범한 실수는 대부분 복구할 수 있지만 되돌린 것은 복구할 수 없다.

종종 완료한 커밋을 수정해야 할 때가 있다. 너무 일찍 커밋했거나 어떤 파일을 잊었을 때 그리고 커밋 메시지를 잘못 작성했을 때 필요하다. 다시 커밋하고 싶으면 파일 수정 작업을 하고 staging area에 추가한 다음 `--amend` 옵션을 사용하여 커밋을 재작성 할 수 있다.
```bash
$ git commit --amend
```

이 명령은 staging area를 사용하여 커밋한다. 만약 마지막으로 커밋하고 나서 수정한 것이 없다면 (커밋하자마자 바로 이 명령을 실행하는 경우) 조금 전에 한 커밋과 모든 것이 같다. 이때는 커밋 메시지만 수정한다.

편집기가 실행되면 이전 커밋 메시지가 자동으로 포함된다. 메시지를 수정하지 않고 그대로 커밋해도 기존의 커밋을 덮어쓴다.

커밋을 했는데 stage하는 것을 잊고 빠트린 파일이 있으면 아래와 같이 고칠 수 있다.
```bash
$ git commit -m 'initial commit'
$ git add forgotten_file
$ git commit --amend
```

위에서 실행한 3 명령어는 모두 커밋 한 개로 기록된다. 두 번째 커밋은 첫 번째 커밋을 덮어쓴다.

**Note**: 이렇게 `--amend` 옵션으로 커밋을 고치는 작업은, 추가로 작업한 일이 작다고 하더라도 이전의 커밋을 완전히 새로 고쳐서 새 커밋으로 변경하는 것을 의미한다. 이전의 커밋은 일어나지 않은 일이 되는 것이고 당연히 히스토리에도 남지 않는다. <br>
`--amend` 옵션으로 커밋을 고치는 작업이 주는 장점은 마지막 커밋 작업에서 아주 뭔가 빠뜨린 작은 것을 넣거나 변경하는 것을 새 커밋으로 분리하지 않고 하나의 커밋에서 처리하는 것이다. “앗차, 빠진 파일 넣었음”, “이전 커밋에서 오타 살짝 고침” 등의 커밋을 만들지 않겠다는 말이다.

### 파일 상태를 unstage로 변경하기 <a id="unstaging-staged-files"></a>
다음은 staging area와 워킹 디렉토리 사이를 넘나드는 방법을 설명한다. 두 영역의 상태를 확인할 때마다 변경된 상태를 되돌리는 방법을 알려주기 때문에 매우 편리하다. 예를 들어 파일을 두 개 수정하고서 따로따로 커밋하려고 했지만, 실수로 `git add *` 라고 실행했다. 두 파일 모두 staging area에 들어 있다. 이제 둘 중 하나를 어떻게 되돌릴까? 우선 `git status` 명령으로 확인해 보자.

```bash
$ git add *
$ git status
On branch main
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    renamed:    README.md -> README
    modified:   CONTRIBUTING.md
```

`Changes to be commited` 아래 `git reset HEAD <file>...`​ 메시지가 보인다. 이 명령으로 unstaged 상태로 변경할 수 있다. `CONTRIBUTING.md` 파일을 unstaged 상태로 변경해 보자.
```bash
$ git reset HEAD CONTRIBUTING.md
Unstaged changes after reset:
M	CONTRIBUTING.md
$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    renamed:    README.md -> README

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

    modified:   CONTRIBUTING.md
```

명령어가 낮설게 느껴질 수도 있지만 잘 동작한다. `CONTRIBUTING.md` 파일은 unstaged 상태가 됐다.

**Note**: `git reset` 명령은 매우 위험하다. `--hard` 옵션과 함께 사용하면 더욱 위험하다. 하지만 위에서 처럼 옵션 없이 사용하면 워킹 디렉토리의 파일은 건드리지 않는다.

지금까지 살펴본 내용이 `git reset` 명령에 대해 알아야 할 대부분의 내용이다. `reset` 명령이 정확히는 어떻게 동작하는지, 어떻게 전문적으로 활용하는지는 [Reset Demystified](https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified#_git_reset) 부분을 참고하도록 한다.


### Modified 파일 되돌리기 <a id="unmodifying-modified-files"></a>
어떻게 해야 `CONTRIBUTING.md` 파일을 수정하고 나서 다시 되돌릴 수 있을까? 그러니까 최근 커밋된 버전으로 (아니면 처음 clone했을 때처럼 워킹 디렉토리에 처음 checkout한 그 내용으로) 되돌릴 수 있을까? `git status` 명령이 친절하게 알려준다. 바로 위에 있는 예제에서 unstaged 부분을 보자.
```bash
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

    modified:   CONTRIBUTING.md
```

위의 메시지는 수정한 파일을 되돌리는 방법을 꽤 정확히 알려준다. 알려주는 대로 한 번 해보자.
```bash
$ git checkout -- CONTRIBUTING.md
$ git status
On branch master
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    renamed:    README.md -> README
```

정상적으로 복원된 것을 알 수 있다.

**Important**: `git checkout —- <file>` 명령은 꽤 위험한 명령이라는 것을 알아야 한다. 원래 파일로 덮어썼기 때문에 수정한 내용은 전부 사라진다. 수정한 내용이 진짜 마음에 들지 않을 때만 사용하자.

변경한 내용을 쉽게 버릴수는 없고 하지만 당장은 되돌려야만 하는 상황이라면 stash와 branch를 사용하자. [Git 브랜치](git-branch.md)에서 다루는 이 방법들이 훨씬 낫다.

Git으로 **커밋**한 모든 것은 언제나 복구할 수 있다. 삭제한 브랜치에 있었던 것도, `--amend` 옵션으로 다시 커밋한 것도 복구할 수 있다 (자세한 것은 [Data Recovery](https://git-scm.com/book/en/v2/ch00/_data_recovery)에 기술되어 있다). 하지만 커밋하지 않고 잃어버린 것은 절대로 되돌릴 수 없다.

### git restore로 되돌리기 <a id="undoing-with-git-resore"></a>
Git 버전 2.23.0은 새로운 명령어 `git restore`를 도입하였다. 이것은 기본적으로 방금 살펴본 `git reset`의 대안이다. Git 버전 2.23.0 이후부터 Git는 많은 실행 취소 작업에 `git reset` 대신 `git restore`를 사용할 수 있게 되었다.

`git reset` 대신 `git restore`를 사용하여 원래대로 되돌려서 실행 취소하도록 한다.

#### git restore로 파일 상태를 unstage로 변경하기 <a id="unstaging-staged-files-with-git-restore"></a>
다음 두 절에서는 `git restore` 명령이 staging area와 워킹 디렉토리의 변경을 어떻게 수행하는 지 보여준다. 좋은 점은 두 영역의 상태를 결정하는 데 사용하는 명령이 변경 내용을 취소하는 방법을 알려준다. 예를 들어, 두 개의 파일을 변경했는데 두 개의 개별 변경으로 커밋하려고 하는데 실수로 `git add *`를 입력하여 두 파일 모두 준비했다고 가정한다. 어떻게 둘 중 하나를 되돌릴 수 있을까? `git status` 명령을 수행해 보자.
```bash
$ git add *
$ git status
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	modified:   CONTRIBUTING.md
	renamed:    README.md -> README
```

"Changes to be committed" 바로 아래 `git restore --stageed <file>...`을 사용하여 unstage한다. 조언을 받아 들여 `CONTRIBUTING.md` 파일을 unstage한다.
```bash
$ git restore --staged CONTRIBUTING.md
$ git status
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	renamed:    README.md -> README

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   CONTRIBUTING.md
```

`CONTRIBUTING.md`파일이 다시 unstaged로 변경되었다.

#### git restore로 modified 파일 되돌리기 <a id="unstaging-staged-files-with-git-restore"></a>
`CONTRIBUTING.md` 파일의 변경을 바라지 않는 다면 어떻게 하여야 할까? 마지막으로 커밋했을 때(또는 처음 복제했을 때 또는 작업 디렉터리에 가져온 방식)로 되돌리려면 어떻게 해야 하나? 다행히 `git status`가 어떻게 하는지 알려준다. 마지막 출력 예제에서, unstage area는 다음과 같다.
```bash
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   CONTRIBUTING.md
```

위의 메시지는 수정한 파일을 되돌리는 방법을 꽤 정확히 알려준다. 알려주는 대로 한 번 해보자.
```bash
$ git restore CONTRIBUTING.md
$ git status
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	renamed:    README.md -> README
```

**Note**: `git restore <file>`은 위험한 명령임을 알고 있어야 한다. 해당 파일에 대한 수정이 모두 사라졌다. Git는 해당 파일을 마지막으로 staging되거나 커밋된 버전으로 교체했다. 저장되지 않은 로컬 수정 사항을 원하지 않음을 확신하지 않는다면 이 명령을 사용하면 안된다.


## 리모트 Repository <a id="remote-repository"></a>
리모트 repository를 관리할 줄 알아야 다른 사람과 함께 협업할 수 있다. 리모트 repository는 인터넷이나 네트워크 어딘가에 있는 repository를 말한다. repository는 여러 개가 있을 수 있는데 어떤 repository는 읽고 쓰기 모두 할 수 있고 어떤 repository는 읽기만 가능할 수 있다. 간단히 말해서 다른 사람들과 함께 일한다는 것은 리모트 repository를 관리하면서 데이터를 그곳에 push하고 pull하는 것이다. 리모트 repository를 관리한다는 것은 repository를 추가, 삭제하는 것뿐만 아니라 브랜치를 관리하고 tracking할지 말지 등을 관리하는 것을 말한다. 이 섹션에서는 리모트 repository를 관리하는 방법에 대해 설명한다.

### 로컬 Repository와 리모트 Repository의 차이<a href="#foot-2" id="foot-2-ref"><sup>2</sup></a>
**로컬 repository**는 **개발자가 사용하는 컴퓨터에 저장된** Git repository이다.

**리모트 repository**는 **원격 시스템(예: [github.com](https://github.com/)과 [gitlab.com](https://gitlab.com/))에 저장된** git repository이다.

통상적으로 협업하는 팀에서는 **central repository**로 리모트 repository를 사용한다. 사용자는 자신의 로컬 repository의 변경 사항을 리모트 repository로 push하고, 또한 팀원들이 변경한 리모트 repository의 변경 사항을 자신의 로컬 repository로 pull하기 위하여 사용된다.

**Workspace**에서 변경 작업을 마쳤으면 **staging(스테이징) 영역**에 추가하고 여기서 로컬 repository에 변경 내용을 커밋할 수 있다. 로컬 repository의 변경 사항을 인터넷에 연결이 안되어 **다른 사용자가 볼 수 없는 경우**에도 작업을 수행할 수 있다.

변경 사항을 공유하려면 로컬 repository에서 **리모트 repository**로 변경 사항을 push한다. 이렇게 하면 **로컬 시스템의 .git 폴더**에서 **원격 시스템의 .git 폴더**로 변경 내용이 복사된다. 이후 리모트 repository에 대한 접근 권한이 있는 사용자에게 변경 사항이 표시된다.

팀원들은 여러분의 변경 내용을 리모트 repository에서 로컬 repository로 가져온 다음 workspace에 통합할 수 있다.

마찬가지로 여러분도 다른 팀원의 변경 사항을 리모트 repository에서 로컬 repository로 가져온 다음 변경 사항을 여러분의 workspace에 통합할 수 있다.

**Note**: **리모트 repository라 하더라도 로컬 시스템에 위치할 수도 있다.**<br>
“리모트” repository라고 이름이 붙어 있어도 이 리모트 repository가 사실 같은 로컬 시스템에 존재할 수도 있다. 여기서 “리모트”라는 이름은 반드시 저장소가 네트워크나 인터넷을 통해 어딘가 멀리 떨어져 있어야만 한다는 것을 의미하지 않는다. 물론 일반적인 리모트 저장소와 마찬가지로 push, pull 등의 기능은 동일하게 사용한다.

### 리모트 Repository 확인 <a id="showing-remotes"></a>
`git remote` 명령으로 현재 프로젝트에 등록된 리모트 repository를 확인할 수 있다. 이 명령은 리모트 repository의 단축 이름을 보여준다. repository를 clone 하면 `origin`이라는 리모트 저장소가 자동으로 등록되기 때문에 `origin`이라는 이름을 볼 수 있다.
```bash
$ git clone https://github.com/schacon/ticgit
Cloning into 'ticgit'...
remote: Reusing existing pack: 1857, done.
remote: Total 1857 (delta 0), reused 0 (delta 0)
Receiving objects: 100% (1857/1857), 374.35 KiB | 268.00 KiB/s, done.
Resolving deltas: 100% (772/772), done.
Checking connectivity... done.
$ cd ticgit
$ git remote
origin
```

`-v` 옵션을 주어 단축이름과 URL을 함께 볼 수 있다.
```bash
$ git remote -v
origin	https://github.com/schacon/ticgit (fetch)
origin	https://github.com/schacon/ticgit (push)
```

리모트 repository가 여러 개 있다면 이 명령은 등록된 전부를 보여준다. 여러 사람과 함께 작업하는 리모트 repository가 여러개라면 아래와 같은 결과를 얻을 수도 있다.
```bash
$ cd grit
$ git remote -v
bakkdoor  https://github.com/bakkdoor/grit (fetch)
bakkdoor  https://github.com/bakkdoor/grit (push)
cho45     https://github.com/cho45/grit (fetch)
cho45     https://github.com/cho45/grit (push)
defunkt   https://github.com/defunkt/grit (fetch)
defunkt   https://github.com/defunkt/grit (push)
koke      git://github.com/koke/grit.git (fetch)
koke      git://github.com/koke/grit.git (push)
origin    git@github.com:mojombo/grit.git (fetch)
origin    git@github.com:mojombo/grit.git (push)
```

이렇게 리모트 repository가 여러 개 등록되어 있으면 다른 사람이 기여한 내용(contributions)을 쉽게 가져올 수 있다. 어떤 저장소에는 push 권한까지 제공하기도 하지만 일단 이 화면에서 push 가능 권한까지는 확인할 수 없다.

리모트 repository와 데이터를 주고받는데 사용하는 다양한 프로토콜에 대해서는 [Git 서버](git-server.md)에서 자세히 살펴보기로 한다.

### 리모트 Repository 추가 <a id="adding-remote-repository"></a>
이전 섹션에서 `git clone` 명령이 묵시적으로 `origin` 리모트 repository를 어떻게 추가했는지 설명했었지만 수박 겉핥기식으로 살펴봤을 뿐이었다. 여기에서는 리모트 repository를 추가하는 방법을 자세히 설명한다. 기존 워킹 디렉토리에 새 리모트 repository를 쉽게 추가할 수 있는데 `git remote add <shortname> <url>` 명령을 사용한다.
```bash
$ git remote
origin
$ git remote add pb https://github.com/paulboone/ticgit
$ git remote -v
origin	https://github.com/schacon/ticgit (fetch)
origin	https://github.com/schacon/ticgit (push)
pb	https://github.com/paulboone/ticgit (fetch)
pb	https://github.com/paulboone/ticgit (push)
```

이제 URL 대신에 `pb`라는 이름을 사용할 수 있다. 예를 들어 로컬 repository에는 없지만 Paul의 저장소에 있는 것을 가져오려면 아래과 같이 실행한다.
```bash
$ git fetch pb
remote: Counting objects: 43, done.
remote: Compressing objects: 100% (36/36), done.
remote: Total 43 (delta 10), reused 31 (delta 5)
Unpacking objects: 100% (43/43), done.
From https://github.com/paulboone/ticgit
 * [new branch]      main     -> pb/main
 * [new branch]      ticgit     -> pb/ticgit
```

로컬에서 `pb/main`이 Paul의 `main` 브랜치이다. 이 브랜치를 로컬 브랜치중 하나에 merge 하거나 checkout해서 브랜치 내용을 자세히 확인할 수 있다. (브랜치 사용 방법은 [Git 브랜치](git-branch.md)에서 자세히 살펴본다)

### 리모트 Repository에서 Pull과 Fetch <a id="fetching-and-pulling-from remotes"></a>
앞서 설명했듯이 리모트 repository에서 데이터를 가져오려면 간단히 아래와 같이 실행한다.
```bash
$ git fetch <remote>
```

이 명령은 로컬에는 없지만, 리모트 repository에는 있는 데이터를 모두 가져온다. 그러면 리모트 repository의 모든 브랜치를 로컬에서 접근할 수 있어서 언제든지 merge를 하거나 내용을 살펴볼 수 있다.

repository를 clone 하면 명령은 자동으로 리모트 repository를 “origin” 이라는 이름으로 추가한다. 그래서 나중에 `git fetch origin` 명령을 실행하면 clone한 이후에 (혹은 마지막으로 가져온 이후에) 수정된 것을 모두 가져온다. `git fetch` 명령은 리모트 repository의 데이터를 모두 로컬로 가져오지만, 자동으로 merge 하지 않는다. 그래서 로컬에서 하던 작업을 정리하고 나서 수동으로 merge 해야 한다.

현재 브랜치가 리모트 브랜치를 추적하도록 설정된 경우 ([다음 섹션](?)과 [Git 브랜치](git-branch.md)에서 좀더 자세히 살펴본다), 그냥 쉽게 `git pull` 명령으로 리모트 repository 브랜치에서 데이터를 가져올 뿐만 아니라 자동으로 로컬 브랜치와 merge시킬 수 있다. 먼저 `git clone` 명령은 자동으로 로컬의 `main` 브랜치가 리모트 repository의 `main` 브랜치를 추적하도록 한다 (물론 리모트 repository에 `main` 브랜치가 있다는 가정에서). 그리고 `git pull` 명령은 clone한 서버에서 데이터를 가져오고 그 데이터를 자동으로 현재 작업하는 코드와 merge한다.

**Note**: git version 2.27 이후부터, `git pull`은 `pull.rebase` 변수가 설정되지 않은 경우 경고를 표시합니다. Git은 변수를 설정할 때까지 경고를 계속한다. <br>
git의 기본 동작(가능한 경우 fast-forward, merge 커밋 생성)을 원하는 경우 `git config --global pull.rebase "false"`<br>
pull할 때 기본 설정을 변경하려면 `git config --global pull.rebase "true"`.

### 리모트 Repository에 Push <a id="pusning-to-remotes"></a>
프로젝트를 공유하고 싶을 때 upstream repository에 push 할 수 있다. 이 명령은 `git push <remote> <branch>`으로 단순하다. `main` 브랜치를 `origin` 서버에 push 하려면 (다시 말하지만 clone하면 보통 자동으로 `origin` 이름으로 생성된다) 아래와 같이 서버에 push 한다.
```bash
$ git push origin main
```

이 명령은 clone한 리모트 repository에 쓰기 권한이 있고, clone하고 난 이후 아무도 upstream repository에 push 하지 않았을 때만 사용할 수 있다. 다시 말해서 clone한 사람이 여러 명 있을 때, 다른 사람이 push한 후에 push하려고 하면 push할 수 없다. 먼저 다른 사람이 작업한 것을 fetch하여 merge한 다음 push 할 수 있다. [Git 브랜치](git-branch.md)에서 서버에 push하는 방법에 대해 자세히 설명할 것이다.

### 리모트 Repository 살펴보기 <a id="inspecting-remotes"></a>
`git remote show <<remote>>` 명령으로 리모트 repository의 구체적인 정보를 확인할 수 있다. `origin`같은 단축이름으로 이 명령을 실행하면 아래와 같은 정보를 볼 수 있다.
```bash
$ git remote show origin
* remote origin
  Fetch URL: https://github.com/schacon/ticgit
  Push  URL: https://github.com/schacon/ticgit
  HEAD branch: main
  Remote branches:
    main                                 tracked
    dev-branch                           tracked
  Local branch configured for 'git pull':
    main merges with remote main
  Local ref configured for 'git push':
    main pushes to main (up to date)
```

### 리모트 Repository 이름 변경 및 리모트 Repository 삭제 <a id="renaming-and-removing-remotes"></a>
`git remote rename` 명령으로 리모트 repository의 이름을 변경할 수 있다. 예를 들어 `pb` 를 `paul` 로 변경하려면 `git remote rename` 명령을 사용한다.
```bash
$ git remote rename pb paul
$ git remote
origin
paul
```

로컬에서 관리하던 리모트 repository의 브랜치 이름도 바뀐다는 점을 염두에 두자. 여태까지 `pb/main`으로 리모트 repository 브랜치를 사용했으면 이제는 `paul/main`으로 사용해야 한다.

리모트 repository를 삭제해야 한다면 `git remote remove` 또는 `git remote rm` 명령을 사용한다. 서버 정보가 바뀌었을 때, 더는 별도의 mirror가 필요하지 않을 때, 더는 기여자가 활동하지 않을 때 필요하다.
```bash
$ git remote remove paul
$ git remote
origin
```

위와 같은 방법으로 리모트 repository를 삭제하면 해당 리모트 repository에 관련된 추적 브랜치 정보나 모든 설정 내용도 함께 삭제된다.


## 태깅 <a id="tagging"></a>
다른 VCS처럼 Git도 태그를 지원한다. 사람들은 보통 릴리즈할 때 사용한다(v1.0, 등등). 이 섹션에서는 태그를 조회하고 생성하는 법과 태그의 종류를 설명한다.

### 태그 조회 <a id="listing-tags"></a>
우선 `git tag` 명령으로 (옵션 `-l`, `--list`) 이미 만들어진 태그가 있는지 확인할 수 있다.
```bash
$ git tag
v0.1
v1.3
```

이 명령은 알파벳 순서로 태그를 출력한다. 사실 순서는 별로 중요하지 않다.

검색 패턴을 사용하여 태그를 검색할 수 있다. Git 소스 repository는 500여 개의 태그가 있다. 만약 1.8.5 버전의 태그들만 검색하고 싶으면 아래와 같이 실행한다.
```bash
$ git tag -l "v1.8.5*"
v1.8.5
v1.8.5-rc0
v1.8.5-rc1
v1.8.5-rc2
v1.8.5-rc3
v1.8.5.1
v1.8.5.2
v1.8.5.3
v1.8.5.4
v1.8.5.5
```

**Note**: **와일드카드를 사용하여 태그 리스트를 확인하려면 `-l`, `--list` 옵션을 지정**<br>
단순히 모든 태그 목록을 확인하기 위해 `git tag` 명령을 실행했을 때 `-l` 또는 `--list` 옵션이 적용된 것과 동일한 결과가 출력된다.<br>
하지만 와일드카드를 사용하여 태그 목록을 검색하는 경우에는 반드시 `-l` 또는 `--list` 옵션을 같이 사용하여야 원하는 결과를 얻을 수 있다.

### 태그 생성 <a id="creating-tags"></a>
Git의 태그는 **lightweight** 태그와 **annotated** 태그로 두 종류가 있다.

lightweight 태그는 브랜치와 비슷한데 브랜치처럼 가리키는 지점을 최신 커밋으로 이동시키지 않는다. 단순히 특정 커밋에 대한 포인터일 뿐이다.

한편 annotated 태그는 Git 데이터베이스에 태그를 만든 사람의 이름, 이메일과 태그를 만든 날짜, 그리고 태그 메시지도 저장한다. GPG(GNU Privacy Guard)로 서명할 수도 있다. 일반적으로 annotated 태그를 만들어 이 모든 정보를 사용할 수 있도록 하는 것이 좋다. 하지만 임시로 생성하는 태그거나 이러한 정보를 유지할 필요가 없는 경우에는 lightweight 태그를 사용할 수도 있다.

### Annotated 태그 <a id="annotated-tags"></a>
annotated 태그를 만드는 방법은 간단하다. `tag` 명령을 실행할 때 `-a` 옵션을 추가한다.
```bash
$ git tag -a v1.4 -m "my version 1.4"
$ git tag
v0.1
v1.3
v1.4
```

`-m` 옵션으로 태그를 저장할 때 메시지를 함께 저장할 수 있다. 명령을 실행할 때 메시지를 입력하지 않으면 Git은 편집기를 실행시킨다.

`git show` 명령으로 태그 정보와 커밋 정보를 모두 확인할 수 있다.
```bash
$ git show v1.4
tag v1.4
Tagger: Ben Straub <ben@straub.cc>
Date:   Sat May 3 20:19:12 2014 -0700

my version 1.4

commit ca82a6dff817ec66f44342007202690a93763949
Author: Scott Chacon <schacon@gee-mail.com>
Date:   Mon Mar 17 21:52:11 2008 -0700

    changed the version number
```

커밋 정보를 보여주기 전에 먼저 태그를 만든 사람이 누구인지, 언제 태그를 만들었는지, 그리고 태그 메시지가 무엇인지 보여준다.


### Lightweight 태그 <a id="lightweight-tags"></a>
lightweight 태그는 기본적으로 파일에 커밋 체크섬을 저장하는 것뿐이다. 다른 정보는 저장하지 않는다. lightweight 태그를 만들 때는 `-a`, `-s`, `-m` 옵션을 사용하지 않는다. 이름만 달아줄 뿐이다.
```bash
$ git tag v1.4-lw
$ git tag
v0.1
v1.3
v1.4
v1.4-lw
v1.5
```

이 태그에 `git show` 를 실행하면 별도의 태그 정보를 확인할 수 없다. 이 명령은 단순히 커밋 정보만을 보여준다.
```
$ git show v1.4-lw
commit ca82a6dff817ec66f44342007202690a93763949
Author: Scott Chacon <schacon@gee-mail.com>
Date:   Mon Mar 17 21:52:11 2008 -0700

    changed the version number
```


### 나중에 태그하기 <a id="tagging-later"></a>
예전 커밋에 대해서도 태그할 수 있다. 커밋 히스토리는 아래와 같다고 가정한다.
```bash
$ git log --pretty=oneline
15027957951b64cf874c3557a0f3547bd83b3ff6 Merge branch 'experiment'
a6b4c97498bd301d84096da251c98a07c7723e65 beginning write support
0d52aaab4479697da7686c15f77a3d64d9165190 one more thing
6d52a271eda8725415634dd79daabbc4d9b6008e Merge branch 'experiment'
0b7434d86859cc7b8c3d5e1dddfed66ff742fcbc added a commit function
4682c3261057305bdd616e23b64b0857d832627b added a todo file
166ae0c4d3f420721acbb115cc33848dfcc2121a started write support
9fceb02d0ae598e95dc970b74767f19372d61af8 updated rakefile
964f16d36dfccde844893cac5b347e7b3d44abbc commit the todo
8a5cbc430f1a9c3d00faaeffd07798508422908a updated readme
```

“updated rakefile” 커밋을 v1.2로 태그하지 못했다고 해도 나중에 태그를 붙일 수 있다. 특정 커밋에 태그하기 위해서 명령의 끝에 커밋 체크섬을 명시한다 (긴 체크섬을 전부 사용할 필요는 없다).
```bash
$ git tag -a v1.2 9fceb02
```

이제 아래와 같이 만든 태그를 확인한다.
```bash
$ git tag
v0.1
v1.2
v1.3
v1.4
v1.4-lw
v1.5

$ git show v1.2
tag v1.2
Tagger: Scott Chacon <schacon@gee-mail.com>
Date:   Mon Feb 9 15:32:16 2009 -0800

version 1.2
commit 9fceb02d0ae598e95dc970b74767f19372d61af8
Author: Magnus Chacon <mchacon@gee-mail.com>
Date:   Sun Apr 27 20:43:35 2008 -0700

    updated rakefile
...
```


### 태그 공유 <a id="sharing-tags"></a>
`git push` 명령은 자동으로 리모트 서버에 태그를 전송하지 않는다. 태그를 만들었으면 서버에 별도로 push 해야 한다. 브랜치를 공유하는 것과 같은 방법으로 할 수 있다. `git push origin <tagname>`을 실행한다.
```bash
$ git push origin v1.5
Counting objects: 14, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (12/12), done.
Writing objects: 100% (14/14), 2.05 KiB | 0 bytes/s, done.
Total 14 (delta 3), reused 0 (delta 0)
To git@github.com:schacon/simplegit.git
 * [new tag]         v1.5 -> v1.5
```

만약 한 번에 태그를 여러 개 push 하고 싶으면 `--tags` 옵션을 추가하여 `git push` 명령을 실행한다. 이 명령으로 리모트 서버에 없는 태그를 모두 전송할 수 있다. 
```bash
$ git push origin --tags
Counting objects: 1, done.
Writing objects: 100% (1/1), 160 bytes | 0 bytes/s, done.
Total 1 (delta 0), reused 0 (delta 0)
To git@github.com:schacon/simplegit.git
 * [new tag]         v1.4 -> v1.4
 * [new tag]         v1.4-lw -> v1.4-lw
```

이제 누군가 저장소에서 clone하거나 pull하면 모든 태그 정보도 함께 전송된다.

**Note**: **`git push` 두 종류 태그 모두를 push한다.**<br>
`git push <remote> --tag`는 lightweight 태그와 annotated 태그를 모두 푸시한다. 현재 lightweight 태그만 push할 수 있는 옵션은 없지만 `git push <remote> -- follow-tags`를 사용하여 annotated 태그만 리모트에 푸시할 수 있다.


### 태그 삭제 <a id="deleting-tags"></a>
`git tag -d <tagname>` 명령을 사용하여 로컬 repository에서 태그를 삭제할 수 있다. 예를 들어, 위의 lightweight 태그를 다음과 같이 제거할 수 있다.
```bash
$ git tag -d v1.4-lw
Deleted tag 'v1.4-lw' (was e7d5add)
```

이렇게 해도 리모트 서버에서 태그가 제거되지는 않는다. 리모트 서버에서 태그를 삭제하는 일반적인 방법은 두 가지가 있다.

첫 번째 변형은 `git push <remote> :refs/tags/<tagname>`이다.
```bash
$ git push origin :refs/tags/v1.4-lw
To /git@github.com:schacon/simplegit.git
 - [deleted]         v1.4-lw
```

위의 해석 방법은 콜론 앞의 null 값으로 읽고, 리모트 태그 이름으로 푸시하여 효과적으로 삭제하는 것이다.

원격 태그를 삭제하는 두 번째(그리고 더 직관적인) 방법은 다음과 같습니다.
```bash
$ git push origin --delete <tagname>
```


### 태그 Checkout <a id="checkingout-tags"></a>
예를 들어 태그가 특정 버전을 가리키고 있고, 특정 버전의 파일을 체크아웃 해서 확인하고 싶다면 다음과 같이 실행한다. 단 태그를 체크아웃하면 (브랜치를 체크아웃 하는 것이 아니라면) “detached HEAD” 상태가 되며 일부 Git 관련 작업이 브랜치에서 작업하는 것과 다르게 동작할 수 있다.
```bash
$ git checkout v2.0.0
Note: switching to 'v2.0.0'.

You are in 'detached HEAD' state. You can look around, make experimental
changes and commit them, and you can discard any commits you make in this
state without impacting any branches by performing another checkout.

If you want to create a new branch to retain commits you create, you may
do so (now or later) by using -c with the switch command. Example:

  git switch -c <new-branch-name>

Or undo this operation with:

  git switch -

Turn off this advice by setting config variable advice.detachedHead to false

HEAD is now at 99ada87... Merge pull request #89 from schacon/appendix-final

$ git checkout v2.0-beta-0.1
Previous HEAD position was 99ada87... Merge pull request #89 from schacon/appendix-final
HEAD is now at df3f601... Add atlas.json and cover image
```

“detached HEAD” 상태에서는 작업을 하고 커밋을 만들면, 태그는 그대로 있으나 새로운 커밋이 하나 쌓인 상태가 되고 새 커밋에 도달할 수 있는 방법이 따로 없게 된다. 물론 커밋의 해시 값을 정확히 기억하고 있으면 가능하긴 하다. 특정 태그의 상태에서 새로 작성한 커밋이 버그 픽스와 같이 의미있도록 하려면 반드시 브랜치를 만들어서 작업하는 것이 좋다.
```bash
$ git checkout -b version2 v2.0.0
Switched to a new branch 'version2'
```

물론 이렇게 브랜치를 만든 후에 `version2` 브랜치에 커밋하면 브랜치는 업데이트된다. 하지만, `v2.0.0` 태그가 가리키는 커밋이 변하지 않았으므로 두 내용이 가리키는 커밋이 다르다는 것을 알 수 있다.


## Git Alias <a id="git-alias"></a>
Git의 기초를 마치기 전에 Git을 좀 더 쉽고 편안하게 쓸 수 있게 만들어 줄 alias라는 팁 알려주려 한다. 우리는 앞으로 이 팁을 다시 거론하지 않고 이런 팁을 알고 있다고 가정한다. 그래서 알고 있는 것이 좋다.

명령을 완벽하게 입력하지 않으면 Git은 알아듣지 못한다. Git의 명령을 전부 입력하는 것이 귀찮다면 `git config`를 사용하여 각 명령의 alias을 쉽게 설정할 수 있다. 아래는 alias을 만드는 예이다.
```bash
$ git config --global alias.co checkout
$ git config --global alias.br branch
$ git config --global alias.ci commit
$ git config --global alias.st status
```

이제 `git commit` 대신 `git ci` 만으로도 커밋할 수 있다. Git을 계속 사용한다면 다른 명령어도 자주 사용하게 될 것이다. 주저말고 자주 사용하는 명령을 alias하여 편하게 사용하길 바란다.

이미 있는 명령을 편리하고 새로운 명령으로 만들어 사용할 수 있다. 예를 들어 파일을  unstaged 상태로 변경하는 명령을 만들어서 불편함을 덜 수 있다. 아래와 같이 unstage 라는 alias을 만든다.
```bash
$ git config --global alias.unstage 'reset HEAD --'
```

아래 두 명령은 동일한 명령이다.
```bash
$ git unstage fileA
$ git reset HEAD -- fileA
```

한결 간결해졌다. 추가로 `last` 명령을 만들어 보자.
```bash
$ git config --global alias.last 'log -1 HEAD'
```

이제 최근 커밋을 좀 더 쉽게 확인할 수 있다.
```bash
$ git last
commit 66938dae3329c7aebe598c2246a8e6af90d04646
Author: Josh Goebel <dreamer3@example.com>
Date:   Tue Aug 26 19:48:51 2008 +0800

    test for current head

    Signed-off-by: Scott Chacon <schacon@example.com>
```

이것으로 쉽게 새로운 명령을 만들 수 있다. 그리고 Git의 명령어뿐만 아니라 외부 명령어도 실행할 수 있다. `!`를 행 앞에 추가하면 외부 명령을 실행한다. 커스텀 스크립트를 만들어서 사용할 때 매우 유용하다. 아래 명령은 `git visual`이라고 입력하면 `gitk`가 실행된다.
```bash
$ git config --global alias.visual '!gitk'
```

## 요약 <a id="summary"></a>
이제 로컬에서 사용할 수 있는 Git 명령에 대한 기본 지식은 갖추었다. repository를 만들고 clone하는 방법, 수정하고 나서 stage하고 커밋하는 방법, repository의 히스토리를 조회하는 방법 등을 살펴보았다. 이어지는 페이지에서 Git의 가장 강력한 기능인 [브랜치 모델](git-branch.md)을 살펴볼 것이다.


---
<a id="foot-1" href="#foot-1-ref"><sup>1</sup></a>[GitHub to replace 'master' with 'main' starting next month](https://www.zdnet.com/article/github-to-replace-master-with-main-starting-next-month/)

<a id="foot-2" href="#foot-2-ref"><sup>2</sup></a>[What is a difference between local and remote repository?](https://www.bettercoder.io/job-interview-questions/532/what-is-a-difference-between-local-and-remote-repository)

