# Git 서버
이 페이지를 읽기 시작했다면 이미 하루 업무의 대부분을 Git으로 처리할 수 있을 거라고 생각한다. 이제는 다른 사람과 협업하는 방법을 고민해보자. 다른 사람과 협업하려면 리모트 repository가 필요하다. 물론 혼자서 repository를 만들고 거기에 push하고 pull할 수도 있지만 이렇게 하는 것은 아무 의미가 없다. 이런 방식으로는 다른 사람이 무슨 일을 하고 있는지 알려면 항상 지켜보고 있어야 간신히 알 수 있을 것이다. 당신이 오프라인일 때도 동료가 repository를 사용할 수 있도록 하려면 언제나 이용할 수 있는 repository가 필요하다. 즉, 공동으로 사용할 수 있는 repository를 만들고 모두 이 repository에 접근하여 push와 pull할 수 있어야 한다.

Git 서버를 운영하는 건 매우 간단하다. 우선 사용할 전송 프로토콜부터 정한다. 이 페이지의 앞부분에서는 어떤 프로토콜이 있는지 그리고 각 장단점은 무엇인지 살펴본다. 그 다음 각 프로토콜을 사용하는 방법과 그 프로토콜을 사용할 수 있도록 서버를 구성하는 방법을 살펴본다<a href="#foot-1" id="foot-1-ref"><sup>1</sup></a>. 마지막으로 다른 사람의 서버에 내 코드를 맡기긴 싫고 고생스럽게 서버를 설치하고 관리하고 싶지도 않을 때 고를 수 있는 선택지가 어떤 것들이 있는지 살펴본다.

서버를 직접 설치해서 운영할 생각이 없으면 이 장의 마지막 절만 읽어도 된다<a href="#foot-2" id="foot-2-ref"><sup>2</sup></a>. 마지막 절에서는 Git 호스팅 서비스에 계정을 만들고 사용하는 방법에 대해 설명한다. 그리고 다음 장에서는 [분산 환경에서 소스를 관리](distributed-git.md)하는 다양한 패턴에 대해 논의할 것이다.

리모트 repository는 일반적으로 워킹 디렉토리가 없는 **bare repository**이다. 이 저장소는 협업용이기 때문에 체크아웃이 필요 없다. 그냥 Git 데이터만 있으면 된다. 다시 말해서 bare repository는 일반 프로젝트에서 `.git` 디렉토리만 있는 repository이다.

## 프로토콜 <a id="protocols"></a>
![underconstruction](../images/under-construction.jpeg)

## 서버에 Git 설치 <a id="getting-git-on-server"></a>
![underconstruction](../images/under-construction.jpeg)

## SSH 공개키 생성 <a id="generating-ssh-public-key"></a>
![underconstruction](../images/under-construction.jpeg)

## 서버 설정 <a id="setting-up-server"></a>
![underconstruction](../images/under-construction.jpeg)
 
## Git 데몬 <a id="git-daemon"></a>
![underconstruction](../images/under-construction.jpeg)

## 스마트 HTTP <a id="smart-http"></a>
![underconstruction](../images/under-construction.jpeg)

## GitWeb <a id="gitweb"></a>
![underconstruction](../images/under-construction.jpeg)

## GitLab <a id="gitlab"></a>
![underconstruction](../images/under-construction.jpeg)

## 또 다른 선택지, 호스팅 <a id="thirf-party-hosted-options"></a>
Git 서버를 직접 운영하기가 부담스러울 수 있다. 이를 위해 여러 Git 호스팅 서비스가 있다. 외부 Git 호스팅 서비스를 사용하면 좋은 점이 있다. 설정이 쉽고 서버 관리 비용을 아낄 수 있다. 내부적으로 Git 서버를 운영하더라도 소스 코드를 공개하기 위해 호스팅 서비스를 이용해야 할 수 있다. 이런 식으로 손쉽게 오픈 소스 커뮤니티를 구성한다.

현재 많은 호스팅 서비스가 있다. 각각 장단점이 있으므로 잘 선택해서 사용하면 된다. [이 페이지](https://git.wiki.kernel.org/index.php/GitHosting)에서 최신 Git 호스팅 서비스 리스트를 찾아 볼 수 있다.

가장 큰 Git 호스팅 서비스인 GitHub에 대해서는 [GitHub](https://git-scm.com/book/ko/v2/ch00/ch06-github)에서 자세히 설명되어 있다. 

## 요약 <a id="summary"></a>
Git 서버를 운영하거나 사람들과 협업을 하는 방법 몇 가지를 살펴 보았다.

자신의 서버에서 Git 서버를 운영하면 제어 범위가 넓어지고 방화벽 등을 운영할 수 있다. 하지만 설정하고 유지보수하는 데에 많은 시간이 든다. 호스팅 서비스를 이용하면 설정과 유지보수가 쉬워진다. 대신 코드를 외부에 두게 된다. 자신의 회사나 조직에서 이를 허용하는지 사용하기 전에 확인해야 한다.

필요에 따라 둘 중 하나를 선택하든지, 아니면 두 방법을 적절히 섞어서 사용하는 것이 좋다.


---
<a id="foot-1" href="#foot-1-ref"><sup>1</sup></a> 개발팀의 팀원은 여기까지는 꼭 필요하지 않으나, 미래에 관리자가 될 것에 대비하여 알아두는 것도 바쁘지 않다고 생각한다.

<a id="foot-2" href="#foot-2-ref"><sup>2</sup></a> 첫 버전에서는 이 부분만 작성하였다.
