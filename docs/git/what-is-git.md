# Git의 개념

Git repository는 프로젝트의 변경된 데이터를 저장하고 있는 특수한 데이터베이스이다<a href="#foot-1" id="foot-1-ref"><sup>1</sup></a>.

데이터베이스는 프로젝트의 현재 버전과 이전 버전의 모든 파일, 모든 디렉토리 및 모든 변경사항의 변경자 및 변경 시기와 관련된 정보를 저장하고 있다.

- 프로젝트의 repository에 접근하면 모든 파일의 원본과 프로젝트의 전체 이력을 볼 수 있다.
- 파일을 변경하고 변경 내용을 제출(submit)하면 Git 버전 제어 시스템(version control system)이 변경 내용을 repository에 저장한다.

## Git 기본 개념 <a href="#foot-2" id="foot-2-ref"><sup>2</sup></a>
Git의 핵심은 무엇일까? 이 질문은 Git을 이해하는데 매우 중요하다. Git이 무엇이고 어떻게 동작하는지 이해한다면 보다 쉽게 Git을 효과적으로 사용할 수 있다. Git을 배우려면 Subversion이나 Perforce 같은 다른 VCS를 사용하던 경험을 버려야 한다. Git은 미묘하게 달라서 다른 VCS에서 쓰던 개념이 혼동을 초래할 수 있다. 사용자 인터페이스는 매우 비슷하지만, 정보를 취급하는 방식이 다르다. 이런 차이점을 이해하면 Git 사용에 보다 쉽게 다가갈 수 있다.

### 차이가 아니라 스냅샷
Subversion과 비슷한 VCS들과 Git의 가장 큰 차이점은 데이터를 다루는 방법에 있다. 큰 틀에서 봤을 때 VCS 시스템 대부분은 관리하는 정보가 파일들의 목록이다. CVS, Subversion, Perforce, Bazaar 등의 시스템은 각 파일의 변화를 시간순으로 저장하여 파일들의 집합으로 관리한다 (보통 델타 기반 버전 관리 시스템이라 한다).

![Figure 1-1](../images/fig-1.png)<br>
Figure 1-1. 델타 기반 버전 관리 시스템<br>
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F)</sup></p>

Git은 위 그림과 같이 데이터를 저장하지 않고 취급하지도 않는다. 대신 Git은 데이터를 프로젝트(폴더)의 연속적인 스냅샷으로 취급하고 크기가 아주 작다. Git은 커밋하거나 프로젝트의 상태를 저장할 때마다 파일이 존재하는 그 순간을 중요하게 여긴다. 파일이 달라지지 않았으면 Git은 성능을 위해서 파일을 새로 저장하지 않는다. 단지 이전 상태의 파일에 대한 링크만 저장한다. Git은 데이터를 스냅샷의 스트림처럼 취급한다.

![Figure 1-2](../images/fig-2.png)<br>
Figure 1-2. 시간순으로 프로젝트의 스냅샷을 저장<br>
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F)</sup></p>

이것이 Git이 다른 VCS와 차별되는 점이다. 이점 때문에 Git은 다른 시스템들이 과거로부터 답습해왔던 버전 컨트롤의 개념과 다르다는 것이고 많은 부분을 새로운 관점에서 바라보아야 한다. 이점이 Git을 단순한 VCS를 넘어 강력한 도구를 지원하는 작은 파일시스템으로 취급할 수 있도록 한다. Git을 사용하여 얻을 수 있는 장점의 예를 [Git 브랜치](git-branch.md)에서 찾아 볼 수 있다.

### 거의 모든 명령을 로컬에서 실행
대부분 명령이 로컬 파일과 데이터에서 작동하기 때문에 많은 경우 인터넷 연결이 필요하지 않다. 대부분의 명령어가 네트워크의 속도에 영향을 받는 VCS에 익숙하다면 Git의 속도에 놀랄 것이다. Git의 이런 특징에서 나오는 미칠듯한 속도는 오직 Git만이 제공할 수 있는 능력이다. 프로젝트의 모든 이력이 로컬 디스크에 저장되어 있기 때문에 모든 명령을 순식간에 실행한다.

예를 들어 Git은 프로젝트의 이력을 조회할 때 서버 연결이 필요하지 않다. 로컬 데이터베이스에서 이력을 읽어 보여 준다. 그결과 눈 깜짝할 사이에 이력을 조회할 수 있다. 어떤 파일의 현재 버전과 한 달 전의 상태를 비교해 보고 싶다면 Git은 단순히 한 달 전의 파일과 지금의 파일을 로컬에서 찾는다. 파일을 비교하기 위해 리모트에 있는 서버에 접근하여 예전 버전을 가져올 필요가 없는 것이다.

즉 오프라인 상태이거나 VPN에 연결하지 못해도 계속해서 일할 수 있다. 인터넷이 연결되지 않은 공간인 비행기나 기차 등에서 작업하고 커밋할 수 있다 (로컬 저장소라는 점을 기억하자). 다른 VCS 시스템에서는 불가능한 일이다. Perforce를 예로 들자면 서버에 연결할 수 없을 때 할 수 있는 일이 별로 없다. Subversion이나 다른 VCS에서도 마찬가지다. 오프라인이기 때문에 데이터베이스에 접근할 수 없어서 파일을 편집할 수는 있지만, 커밋할 수 없다. 매우 사소해 보이지만 실제로 이 상황에 맞닥드리면 느껴지는 차이는 매우 클 것이다.

### Git의 무결성
Git은 데이터를 저장하기 전에 항상 체크섬을 생성하고 그 체크섬으로 데이터를 관리한다. 따라서 체크섬을 인식하는 Git 없이는 어떠한 파일이나 디렉토리도 변경할 수 없다. 체크섬은 Git에서 사용하는 가장 기본적인(Atomic) 데이터 단위이자 Git의 기본 철학이다. Git 없이는 체크섬을 다룰 수 없어서 파일의 상태도 알 수 없고 심지어 데이터를 잃어버릴 수도 없다.

Git은 SHA-1 해시를 사용하여 체크섬을 생성한다. 생성된 체크섬은 40자 길이의 16진수 문자열이다. 파일의 내용이나 디렉토리 구조를 이용하여 체크섬을 구한다. SHA-1은 아래와 같다.
```
24b9da6552252987aa493b52f8696cd6d3b00373
```

Git은 모든 것을 해시로 식별하기 때문에 이런 값은 자주 볼 수 있다. 실제로 Git은 데이터베이스에 파일 이름으로 저장하지 않고 해당 파일의 해시로 저장한다.

### Git은 오직 데이터를 추가할뿐
Git의 모든 명령은 Git 데이터베이스에 데이터로 추가된다. 되돌리거나 데이터를 삭제할 방법은 없다. 다른 VCS처럼 Git도 커밋하지 않으면 변경사항을 잃어버릴 수 있다. 그러나 일단 스냅샷을 커밋하고 나면 데이터를 잃어버리지는 않는다. 특히 데이터베이스를 정기적으로 다른 리포지토리로 push하는 경우에는 더욱 그러하다.

Git을 사용하면 프로젝트가 심각하게 망가질 걱정없이 매우 편하게 여러 가지 실험을 해 볼 수 있다. [되돌리기](../getting-started-git/#undoing-things)을 보면 Git에서 데이터를 어떻게 저장하고 손실을 어떻게 복구하는지 자세히 알 수 있다.

### 세 가지 상태 <a id="three-states"></a>
이 부분은 중요하여 꼭 기억하여야 한다. Git을 사용하기 위해 반드시 꼭 알아야 할 부분이다. Git은 파일을 **modified**, **staged**와  **committed** 세 가지 상태로 관리한다.

- Modified는 파일을 수정하였고 아직 로컬 데이터베이스에 커밋하지 않은 상태를 말한다.
- Staged란 현재 수정한 파일을 곧 커밋할 것이라고 표시한 상태를 의미한다.
- Committed란 데이터가 로컬 데이터베이스에 안전하게 저장됐다는 것을 의미한다.

위의 세 상태는 Git 프로젝트의 세 단계 working tree(workspace), staging area 및 Git 디렉토리에 연결된다. 

![Figure 1-3](../images/fig-3.png)<br>
Figure 1-3. Workspace, Staging Area와 Git repository
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F)</sup></p>

working tree는 프로젝트의 특정 버전을 checkout하는 것이다. Git 디렉토리에 기록된 압축된 데이터베이스에서 파일을 가져와서(pull) working tree를 사용하여 파일들을 변경할 수 있도록 디스크에 구성한다.

staging area는 Git 디렉토리에 있는 단순한 파일로 곧 커밋할 파일에 대한 정보를 저장한다. Git 용어로 “index” 라고 하지만, “staging area” 라는 용어를 사용하기도 한다.

Git은 프로젝트의 메타데이터와 객체 데이터베이스를 Git 디렉토리에 저장한다. Git 디렉토리가 Git의 핵심이다. 다른 컴퓨터에 있는 repository를 **clone**하면 Git 디렉토리가 만들어진다.

Git으로 하는 일은 기본적으로 아래와 같다.

1. working tree에서 파일을 수정한다.
1. staging area에 파일을 stage하여 커밋할 스냅샷을 만든다. 모든 파일을 추가할 수도 있고 선택하여 추가할 수도 있다.
1. staging area에 있는 파일들을 커밋해서 Git 디렉토리에 영구적인 스냅샷으로 저장한다.

Git 디렉토리에 있는 파일들은 `committed` 상태이다. 파일을 수정하고 staging area에 추가했다면 `staged` 상태이다. 그리고 checkout한 다음 수정했지만, 아직 staging area에 추가하지 않았다면 `modified` 상태이다. [수정하고 Repository에 저장하기](../getting-started-git/#recording-changes-to-repository)에서 이 상태에 대해 좀 더 자세히 살펴본다. 

---
<a id="foot-1" href="#foot-1-ref"><sup>1</sup></a>[What is a Git repository?](https://www.bettercoder.io/job-interview-questions/529/what-is-a-git-repository)

<a id="foot-2" href="#foot-2-ref"><sup>2</sup></a>[시작하기 - Git 기초](https://git-scm.com/book/ko/v2/%EC%8B%9C%EC%9E%91%ED%95%98%EA%B8%B0-Git-%EA%B8%B0%EC%B4%88)를 복사하여 편집한 것임.
