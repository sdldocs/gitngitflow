# Local Repository와 Remote Repository의 차이<a href="#foot-1" id="foot-1-ref"><sup>1</sup></a>
**Local repository**는 **개발자가 사용하는 컴퓨터에 저장된** Git repository이다.

**Remote repository**는 **원격 시스템(예: [github.com](https://github.com/)과 [gitlab.com](https://gitlab.com/))에 저장된** git repository이다.

통상적으로 협업하는 팀에서는 **central repository**로 remote repository를 사용한다. 사용자는 자신의 local repository의 변경 사항을 remote repository로 푸시하고, 또한 팀원들이 변경한 remote repository의 변경 사항을 자신의 local repository로 가져오기 위하여 사용된다.

**Workspace**에서 변경 작업을 마쳤으면 **staging(스테이징) 영역**에 추가하고 여기서 local repository에 변경 내용을 커밋할 수 있다. 인터넷에 연결이 안되어 local repository의 변경 사항을 **다른 사용자가 볼 수 없는 경우**에도 작업을 수행할 수 있다.

변경 사항을 공유하려면 local repository에서 **remote repository**로 변경 사항을 푸시한다. 이렇게 하면 **로컬 시스템의 .git 폴더**에서 **원격 시스템의 .git 폴더**로 변경 내용이 복사된다. 이후 remote repository에 대한 접근 권한이 있는 사용자에게 변경 사항이 표시된다.

팀원들은 여러분의 변경 내용을 remote repository에서 local repository로 가져온 다음 workspace에 통합할 수 있다.

마찬가지로 여러분도 다른 팀원의 변경 사항을 remote repository에서 local repository로 가져온 다음 변경 사항을 여러분의 workspace에 통합할 수 있다.


---
<a id="foot-1" href="#foot-1-ref"><sup>1</sup></a>[What is a difference between local and remote repository?](https://www.bettercoder.io/job-interview-questions/532/what-is-a-difference-between-local-and-remote-repository)
