이 페이지에서는 [Git 기초]()에서 설명하지 않았지만 이를 보완하기 위한 명령어을 소개한다.

## 최소하기<a href="#foot-1" id="foot-1-ref"><sup>1</sup></a> <a id="undo"></a>
git을 사용할 때 자주 필요한 이전 명령(`git add`, `git commit`와 `git push`)을 취소하는 방법에 대하여 설명한다.

### `git add` 취소 <a id="undo-git-add"></a>
이는 파일의 상태를 Unstage로 변경하기 위한 것이다. 아래와 같이 실수로 `git add *` 명령을 사용하여 모든 파일을 Staging Area에 넣은 경우,
staged 파일 중 빼고 즉 이를 취소하고 싶은 파일이 있을 때가 있다.
```bash
// 모든 파일이 Staged 상태로 바뀐다.
$ git add *
// 파일들의 상태를 확인한다.
$ git status
On branch master
Changes to be committed:
(use "git reset HEAD <file>..." to unstage)
  renamed:    README.md -> README
  modified:   CONTRIBUTING.md
```

이때, `git reset HEAD <file-name>` 명령을 통해 앞선 수행한 `git add *`를 취소할 수 있다. 명령에 `<file-name>`이 없으면 stage한 파일 모두를 unstage한다.
아래와 같이 `CONTRIBUTING.md` 파일을 Unstaged 상태로 변경해보자.
```bash
// CONTRIBUTING.md 파일을 Unstage로 변경한다.
$ git reset HEAD CONTRIBUTING.md
Unstaged changes after reset:
M	CONTRIBUTING.md
```

이후 `git status`를 통해 확인하여 보자.
```bash
// 파일들의 상태를 확인한다.
$ git status
On branch master
Changes to be committed:
(use "git reset HEAD <file>..." to unstage)
  renamed:    README.md -> README
Changes not staged for commit:
(use "git add <file>..." to update what will be committed)
(use "git checkout -- <file>..." to discard changes in working directory)
  modified:   CONTRIBUTING.md
```

### `git commit` 취소 <a id="undo-git-commit"></a>

#### commit 취소
너무 일찍 commit하였거나 파일을 빼먹고 commit한 경우, `git reset HEAD^` 명령을 통해 commit을 취소할 수 있다. 아래와 같이 취소할 수 있다.
```bash
// commit 목록 확인
$ git log
// [방법 1] commit을 취소하고 해당 파일들은 staged 상태로 워킹 디렉터리에 보존
$ git reset --soft HEAD^
// [방법 2] commit을 취소하고 해당 파일들은 unstaged 상태로 워킹 디렉터리에 보존
$ git reset --mixed HEAD^ // 기본 옵션
$ git reset HEAD^ // 위와 동일
$ git reset HEAD~2 // 마지막 2개의 commit을 취소
// [방법 3] commit을 취소하고 해당 파일들은 unstaged 상태로 워킹 디렉터리에서 삭제
$ git reset --hard HEAD^
```

#### commit message 변경
commit message를 잘못 적은 경우, `git commit –amend` 명령을 통해 commit message를 변경할 수 있다.
```bash
$ git commit --amend
```

> **Tips**: `git reset` 명령은 아래의 옵션과 관련해서 주의하여 사용해야 한다.<br>
> –soft : index 보존(add한 상태, staged 상태), 워킹 디렉터리의 파일 보존. 즉 모두 보존. <br>
> –mixed : index 취소(add하기 전 상태, unstaged 상태), 워킹 디렉터리의 파일 보존 (기본 옵션) <br>
> –hard : index 취소(add하기 전 상태, unstaged 상태), 워킹 디렉터리의 파일 삭제. 즉 모두 취소. <br>

> **Tips**: 만약 워킹 디렉터리를 원격 저장소의 마지막 commit 상태로 되돌리고 싶다면, 아래의 명령어를 사용한다.
단, 이 명령을 사용하면 원격 repository에 있는 마지막 commit 이후의 워킹 디렉터리와 add했던 파일들이 모두 사라지므로 주의해야 한다.
```bash
// 워킹 디렉터리를 원격 저장소의 마지막 commit 상태로 되돌린다.
$ git reset --hard HEAD
```

### `git push` 취소 <a id="undo-git-push"></a>
이 명령은 로컬 repository의 내용을 remote repository에 강제로 덮어쓰기를 하는 것이기 때문에 주의해야 한다. 되돌아간 commit 이후의 모든 commit 정보가 사라지기 때문에 주의해야 한다. 특히, 협업 프로젝트에서는 동기화 문제가 발생할 수 있으므로 팀원과 상의 후 진행하는 것이 좋다.

1) 워킹 디렉터리에서 commit 취소한다. 가장 최근의 commit을 취소하고 워킹 디렉터리를 되돌린다. <br>
```bash
// 가장 최근의 commit을 취소 (기본 옵션: --mixed)
$ git reset HEAD^
``` 
<br>
원하는 시점으로 워킹 디렉터리를 되돌린다.
```bash
// Reflog(브랜치와 HEAD가 지난 몇 달 동안에 가리켰었던 커밋) 목록 확인
$ git reflog 또는 $ git log -g
// 원하는 시점으로 워킹 디렉터리를 되돌린다.
$ git reset HEAD@{number} 또는 $ git reset <commit-id>
```

2) 되돌려진 상태에서 다시 commit을 한다.
```bash
$ git commit -m "Write commit messages"
```
3) 원격 저장소에 강제로 push 한다.
```bash
$ git push origin <branch-name> -f
또는
$ git push origin +<branch-name>
```

```bash
// Ex) main branch를 원격 저장소(origin)에 강제로 push
$ git push origin +main
```

> **Tips**: 경고를 무시하고 강제로 push 하기<br>
> `-f` 또는 `–force` 옵션 사용하가나 `+<branch-name>`을 사용하여 해당 브랜치를 강제로 push한다.

### Untracked 파일 삭제 <a id="delete-untracked-files"></a>

`git clean` 명령의 기본은 추적 중이지 않은 파일만 삭제하는 것이다. 즉, `.gitignore`에 명시하여 무시되는 파일은 삭제되지 않는다.
```
$ git clean -f // 디렉터리를 제외한 파일들만 삭제
$ git clean -f -d // 디렉터리까지 삭제
$ git clean -f -d -x // 무시된 파일까지 삭제
```

> **Tips**: 파일을 `.gitignore`에 설정하지 않고 remote repository (여기서는 GitLab)에 push한 경우, 다음과 같이 해결할 수 있다.<br>
> 1 원격 저장소에서 파일 삭제<br>
> 이미 remote repository에 push를 했기 때문에 로컬 repository의 파일을 삭제해도 remote repository에는 삭제되지 않는다. `git rm`와 `git rm –cached` 명령을 사용하여 모두 삭제하여야 한다.<br>
```bash
// 원격 저장소와 로컬 저장소에 있는 파일을 삭제한다.
$ git rm <file-name>
// 원격 저장소에 있는 파일을 삭제한다. 로컬 저장소에 있는 파일은 삭제하지 않는다.
$ git rm --cached <file-name>
``` 
<br>
> 2 `.gitignore` 설정 <br>
> 만약 .gitignore가 제대로 설정되어 있지 않다면 .gitigore 설정하여 다음에는 개인이 관리해야되는 파일들이 원격 저장소에 올라가지 않도록 해야한다. <br>
> 3 원격 저장소에 적용 <br>
> 버전 관리에서 완전히 제거하기 위해서는 반드시 commit과 push 명령을 수행하여야 한다.
```bash
// 버전 관리에서 완전히 제외하기 위해서는 반드시 commit 명령어를 수행해야 한다.
$ git commit -m "Fixed untracked files"
// 원격 저장소(origin)에 push
$ git push origin main
```

## `git reset`에 대하여 <a id="about-git-reset"></a>
앞서 설명한 [취소하기](#undo)에서 `git reset` 명령에 대하여 내부적으로 어떻게 작동하는 지에 대하여 설명한다. reset과 checkout 두 명령은 Git을 처음 사용하는 사람을 가장 헷갈리게 하는 부분이다. 정확히 이해하고 사용할 수 없을 것으로 보일 정도로 많은 기능을 지녔다. 이해하기 쉽게 간단한 비유를 들어 설명하도록 한다.

### 세개의 트리 <a id="3-trees"></a>
Git을 서로 다른 세 트리를 관리하는 컨텐츠 관리자로 생각하면 reset과 checkout을 좀 더 쉽게 이해할 수 있다. 여기서 “트리”란 실제로는 “파일의 컬렉션(collection)”이다. 자료구조의 트리가 아니며, 세 트리 중 Index는 트리 구조가 아니지만, 이해를 쉽게 하려고 일단 트리라고 한다.

Git은 일반적으로 세 트리를 관리하는 시스템이다.

| 트리 | 역할 |
|-----|-----|
| HEAD | 마지막 커밋 스냅샷, 다음 커밋의 부모 커밋 |
| Index | 다음에 커밋할 스냅샷 |
| 워킹 디렉토리 | 샌드박스 |

#### HEAD <a id="head"></a>
HEAD는 현재 checkout된 브랜치의 가장 최신 commit을 가리킨다. 즉 브랜치에 기록된 commit 중 가장 마지막 commit을 가리킨다. 지금의 HEAD가 가리키는 commit은 바로 다음 commit의 부모가 된다. 단순하게 생각하면 HEAD는 **현재 브랜치 마지막 커밋의 스냅샷**이다.

HEAD가 가리키는 스냅샷을 살펴보기는 쉽다. 아래는 HEAD 스냅샷의 디렉토리 리스팅과 각 파일의 SHA-1 체크섬을 보여주는 예제다.

```bash
$ git cat-file -p HEAD
tree cfda3bf379e4f8dba8717dee55aab78aef7f4daf
author Scott Chacon  1301511835 -0700
committer Scott Chacon  1301511835 -0700

initial commit

$ git ls-tree -r HEAD
100644 blob a906cb2a4a904a152...   README
100644 blob 8f94139338f9404f2...   Rakefile
040000 tree 99f1a6d12cb4b6f19...   lib
```

`cat-file`과 `ls-tree` 명령은 일상적으로는 잘 사용하지 않는 저수준(lower level) 명령이다. 이런 저수준 명령을 “plumbing” 명령이라고 한다. Git이 실제로 무슨 일을 하는지 볼 때 유용하다.

#### Index <a id="index"></a>
index는 **바로 다음에 commit**할 것들이다. 이미 [Git의 개념](../what-is-git/)의 [세가지 상태](../what-is-git/#three-states)에서 이런 개념을 “staging area” 라고 설명한 바 있다. “staging area” 는 사용자가 `git commit` 명령을 실행했을 때 Git이 처리할 것들이 있는 곳이다.

먼저 index는 워킹 디렉토리에서 마지막으로 checkout한 브랜치의 파일 목록과 파일 내용으로 채워진다. 이후 파일을 편집하고 수정된 내용으로 index를 업데이트 할 수 있다. 이렇게 업데이트하고 `git commit` 명령을 실행하면 index는 새 commit으로 변환된다.

```bash
$ git ls-files -s
100644 a906cb2a4a904a152e80877d4088654daad0c859 0	README
100644 8f94139338f9404f26296befa88755fc2598c289 0	Rakefile
100644 47c6340d6459e05787f644c2447d2595f5d3a54b 0	lib/simplegit.rb
```

또 다른 저수준 `git ls-files` 명령은 훨씬 더 장막 뒤에 가려져 있는 명령으로 이를 실행하면 현재 index가 어떤 상태인지를 확인할 수 있다.

index는 엄밀히 말해 트리구조는 아니다. 사실 index는 평평한 구조로 구현되어 있다. 여기에서는 쉽게 이해할 수 있도록 그냥 트리라고 설명한다.

#### 워킹 디렉토리 <a id="working-directory"></a>
마지막으로 워킹 디렉토리를 살펴보자. 위의 두 트리는 파일과 그 내용을 효율적인 형태로 .git 디렉토리에 저장한다. 하지만, 사람이 읽기는 어렵다. 워킹 디렉토리는 실제 파일로 존재한다. 바로 눈에 보이기 때문에 사용자가 편집하기 수월하다. 워킹 디렉토리는 샌드박스처럼 생각할 수 있다. commit하기 전에는 index(staging area)에 올려놓고 얼마든지 편집할 수 있다.
```bash
// tree
.
├── README
├── Rakefile
└── lib
    └── simplegit.rb

1 directory, 3 files
```

### 워크플로우 <a id="workflow"></a>
Git의 주목적은 프로젝트의 스냅샷을 지속적으로 저장하는 것이다. 이 트리 세 개를 사용해 더 나은 상태로 관리한다.

![fig 1-73](../images/fig-5-01.png)<br>
Fig 1-73. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

이 과정을 시각화해 보자. 파일이 하나 있는 디렉토리로 이동한다. 이걸 파일의 v1이라고 하고 파란색으로 표시한다. `git init` 명령을 실행하면 Git 저장소가 생기고 HEAD는 아직 없는 `main` 브랜치를 가리킨다.

![fig 1-74](../images/fig-5-02.png)<br>
Fig 1-74. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

이 시점에서는 워킹 디렉토리 트리에만 데이터가 있다.

이제 파일을 commit하여 보자. `git add` 명령으로 워킹 디렉토리의 내용을 index로 복사한다.

![fig 1-75](../images/fig-5-03.png)<br>
Fig 1-75. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

그리고 `git commit` 명령을 실행한다. 그러면 index의 내용을 스냅샷으로 영구히 저장하고 그 스냅샷을 가리키는 commit 객체를 만든다. 그리고는 `main`이 그 commit 객체를 가리키도록 한다.

![fig 1-76](../images/fig-5-04.png)<br>
Fig 1-76. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

이때 git status 명령을 실행하면 아무런 변경 사항이 없다고 나온다. 세 트리 모두가 같기 때문이다.

다시 파일 내용을 바꾸고 commit해보자. 위에서 했던 것과 과정은 비슷하다. 먼저 워킹 디렉토리의 파일을 수정친다. 이를 이 파일의 v2라고 하고 빨간색으로 표시한다.

![fig 1-77](../images/fig-5-05.png)<br>
Fig 1-77. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

`git status` 명령을 바로 실행하면 “Changes not staged for commit,” 아래에 빨간색으로 된 파일을 볼 수 있다. index와 워킹 디렉토리가 다른 내용을 담고 있기 때문에 그렇다. `git add` 명령을 실행해서 변경 사항을 index에 반영하자.

![fig 1-78](../images/fig-5-06.png)<br>
Fig 1-78. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

이 시점에서 `git status` 명령을 실행하면 “Changes to be committed” 아래에 파일 이름이 녹색 박스 내부애 있게 된다. index와 HEAD의 다른 파일들이 여기에 표시된다. 즉 다음 commit할 것과 지금 마지막 commit이 다르다는 말이다. 마지막으로 `git commit` 명령을 실행해 commit한다.

![fig 1-79](../images/fig-5-07.png)<br>
Fig 1-79. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

이제 `git status` 명령을 실행하면 아무것도 출력하지 않는다. 세 개의 트리의 내용이 다시 같아졌기 때문이다.

브랜치를 바꾸거나 Clone 명령도 내부에서는 비슷한 절차를 밟는다. 브랜치를 checkout하면, **HEAD**가 새로운 브랜치를 가리키도록 바뀌고, 새로운 commit의 스냅샷을 **index**로 가져온다. 그리고 **index**의 내용을 **워킹 디렉토리**에 복사한다.

### Reset의 역할 <a id="reset-role"></a>
위의 트리 세 개를 이해하면 `reset` 명령이 어떻게 동작하는지 쉽게 알 수 있다.

예로 들어 `file.txt` 파일 하나를 수정하고 commit한다. 이것을 세 번 반복한 것이다. 그러면 히스토리는 아래와 같이 된다.

![fig 1-80](../images/fig-5-08.png)<br>
Fig 1-80. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

이제 `reset` 명령이 정확히 어떤 일을 하는지 낱낱이 파헤쳐보자. `reset` 명령은 이 세 트리를 간단하고 예측 가능한 방법으로 조작한다. 트리를 조작하는 동작은 세 단계 이하로 이루어진다.

#### 1단계: HEAD 이동
`reset` 명령이 하는 첫 번째 일은 HEAD 브랜치를 이동시킨다. `checkout` 명령처럼 HEAD가 가리키는 브랜치를 바꾸지는 않는다. HEAD는 계속 현재 브랜치를 가리키고 있고, 현재 브랜치가 가리키는 commit을 바꾼다. HEAD가 `main` 브랜치를 가리키고 있다면(즉 `main` 브랜치를 checkout 하고 작업하고 있다면) `git reset 9e5e6a4` 명령은 `main` 브랜치가 `9e5e6a4`를 가리키게 한다.

![fig 1-81](../images/fig-5-09.png)<br>
Fig 1-81. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

`reset` 명령에 commit을 넘기고 실행하면 언제나 이런 작업을 수행한다. `reset --soft` 옵션을 사용하면 딱 여기까지 진행하고 동작을 멈춘다.

이제 위의 다이어그램을 보고 어떤 일이 일어난 것인지 생각해보자. reset 명령은 가장 최근의 `git commit` 명령을 되돌린다. `git commit` 명령을 실행하면 Git은 새로운 커밋을 생성하고 HEAD가 가리키는 브랜치가 새로운 commit을 가리키도록 업데이트한다. `reset` 명령 뒤에 `HEAD~` (HEAD의 부모 커밋)를 주면 index나 워킹 디렉토리는 그대로 놔두고 브랜치가 가리키는 commit만 이전으로 되돌린다. index를 업데이트한 다음에 `git commit` 명령를 실행하면 `git commit --amend` 명령의 결과와 같아진다 ([Changing the Last Commit](https://git-scm.com/book/en/v2/ch00/_git_amend)를 참조).

#### 2단계: Index 업데이트 (--mixed)
여기서 `git status` 명령을 실행하면 index와 `reset` 명령으로 이동시킨 HEAD의 다른 점은 파일이 녹색 박스내에 있는 것이다.

`reset` 명령은 여기서 한 발짝 더 나아가 index를 현재 HEAD가 가리키는 스냅샷으로 업데이트할 수 있다.

![fig 1-82](../images/fig-5-10.png)<br>
Fig 1-82. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

`--mixed` 옵션을 주고 실행하면 `reset` 명령은 여기까지 하고 멈춘다. `reset` 명령을 실행할 때 아무 옵션도 주지 않으면 기본적으로 `--mixed` 옵션으로 동작한다 (예제와 같이 `git reset HEAD~` 처럼 명령을 실행하는 경우).

위의 다이어그램을 보고 어떤 일이 일어날지 한 번 더 생각해보자. 가리키는 대상을 가장 최근의 commit으로 되돌리는 것은 같다. 그러고 나서 **staging area** 를 비우기까지 한다. `git commit` 명령도 되돌리고 `git add` 명령까지 되돌리는 것이다.

#### 3 단계: 워킹 디렉토리 업데이트 (--hard)
`reset` 명령은 세 번째로 워킹 디렉토리까지 업데이트한다. `--hard` 옵션을 사용하면 `reset` 명령은 이 단계까지 수행한다.

![fig 1-83](../images/fig-5-11.png)<br>
Fig 1-83. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

이 과정은 어떻게 동작하는지 가늠해보자. `reset` 명령을 통해 `git add`와 `git commit` 명령으로 생성한 마지막 commit을 되돌린다. **그리고** 워킹 디렉토리의 내용까지도 되돌린다.

이 `--hard` 옵션은 매우 매우 중요하다. `reset` 명령을 위험하게 만드는 유일한 옵션이다. Git에는 데이터를 실제로 삭제하는 방법이 별로 없다. 이 방법은 그 중 하나다. `reset` 명령을 어떻게 사용하더라도 간단히 결과를 되돌릴 수 있다. 하지만 `--hard` 옵션은 되돌리는 것이 불가능하다. 이 옵션을 사용하면 워킹 디렉토리의 파일까지 강제로 덮어쓴다. 이 예제는 파일의 **v3**버전을 아직 Git이 commit으로 보관하고 있기 때문에 `reflog`를 이용해서 다시 복원할 수 있다. 만약 commit한 적 없다면 Git이 덮어쓴 데이터는 복원할 수 없다.

#### 요약 정리
`reset` 명령은 정해진 순서대로 세 개의 트리를 덮어써 나가다가 옵션에 따라 지정한 곳에서 멈춘다.

1. HEAD가 가리키는 브랜치를 옮긴다. **(--soft 옵션이 붙으면 여기까지)**
2. index를 HEAD가 가리키는 상태로 만든다. **(--hard 옵션이 붙지 않았으면 여기까지)**
3. 워킹 디렉토리를 index의 상태로 만든다.

### 경로를 주고 Reset <a id="reset-with-path"></a>
지금까지 `reset` 명령을 실행하는 기본 형태와 사용 방법을 살펴보았다. `reset` 명령을 실행할 때 경로를 지정하면 1단계를 건너뛰고 정해진 경로의 파일에만 나머지 `reset` 단계를 적용한다. 이는 당연한 이야기다. HEAD는 포인터인데 경로에 따라 파일별로 기준이 되는 commit을 부분적으로 적용하는 건 불가능하다. 하지만, index나 워킹 디렉토리는 일부분만 갱신할 수 있다. 따라서 2, 3단계는 가능하다.

예를 들어 `git reset file.txt` 명령을 실행한다고 가정하자. 이 형식은(commit의 해시 값이나 브랜치도 표기하지 않고 `--soft`나 `--hard`도 표기하지 않은) `git reset --mixed HEAD file.txt`를 짧게 쓴 것이다.

1. HEAD의 브랜치를 옮긴다. **(건너뜀)**
2. index를 HEAD가 가리키는 상태로 만든다. **(여기서 멈춤)**

본질적으로는 `file.txt` 파일을 HEAD에서 index로 복사하는 것뿐이다.

![fig 1-84](../images/fig-5-12.png)<br>
Fig 1-84. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

이 명령은 해당 파일을 unstaged 상태로 만든다. 이 명령의 다이어그램과 `git add` 명령을 비교해보면 정확히 반대인 것을 알 수 있다.

![fig 1-85](../images/fig-5-13.png)<br>
Fig 1-85. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

이것이 `git status` 명령에서 이 명령을 보여주는 이유다. 이 명령으로 파일을 unstaged 상태로 만들 수 있다. (더 자세한 내용은 [파일 상태를 unstage로 변경하기](../getting-started-git/#unstaging-staged-files)를 참고한다.)

특정 commit을 명시하면 Git은 “`HEAD에서 파일을 가져오는” 것이 아니라 그 commit에서 파일을 가져온다`. `git reset eb43bf file.txt` 명령과 같이 실행한다.

![fig 1-86](../images/fig-5-14.png)<br>
Fig 1-86. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

이 명령을 실행한 것과 같은 결과를 만들려면 워킹 디렉토리의 파일을 **v1**으로 되돌리고 `git add` 명령으로 index를 **v1**으로 만들고 나서 다시 워킹 디렉토리를 **v3**로 되돌려야 한다 (결과만 같다는 얘기다). 이 상태에서 `git commit` 명령을 실행하면 **v1**으로 되돌린 파일 내용을 기록한다. 워킹 디렉토리를 사용하지 않았다.

`git add` 명령처럼 `reset` 명령도 hunk 단위로 사용할 수 있다. `--patch` 옵션을 사용해서 staging area에서 hunk 단위로 unstaged 상태로 만들 수 있다. 이렇게 선택적으로 unstaged 상태로 만들거나 내리거나 하여 이전 버전으로 복원시킬 수 있다.

### Squash <a id="squash"></a>
여러 commit을 commit 하나로 합치는 흥미로운 도구를 알아보자.

“oops.”, “WIP” 또는 “forgot this file” 같은 깃털같이 가벼운 commit들이 있다고 해보자. 이럴 때는 `reset` 명령으로 commit들을 하나로 합쳐서 남들에게 똑똑한 척할 수 있다. ([Squashing Commits](https://git-scm.com/book/en/v2/ch00/_squashing)를 하는 명령어가 따로 있지만, 여기서는 `reset` 명령을 쓰는 것이 더 간단할 때도 있다는 것을 보여준다.)

다음과 같은 프로젝트가 있다고 생각해보자. 첫 번째 commit은 파일 하나를 추가했고, 두 번째 commit은 기존 파일을 수정하고 새로운 파일도 추가했다. 세 번째 commit은 첫 번째 파일을 다시 수정했다. 두 번째 commit은 아직 작업 중인 commit으로 이 commit을 세 번째 commit과 합치고 싶은 상황이다.

![fig 1-87](../images/fig-5-15.png)<br>
Fig 1-87. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

`git reset --soft HEAD~2` 명령을 실행하여 HEAD 포인터를 이전 commit으로 되돌릴 수 있다 (히스토리를 그대로 처음처럼 유지할 commit으로).

![fig 1-88](../images/fig-5-16.png)<br>
Fig 1-88. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

이 상황에서 `git commit` 명령을 실행한다.

![fig 1-89](../images/fig-5-17.png)<br>
Fig 1-89. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

이제 사람들에게 공개할만한 히스토리가 만들어졌다. `file-a.txt` 파일이 있는 v1 commit이 하나 그대로 있고, 두 번째 commit에는 v3버전의 `file-a.txt` 파일과 새로 추가된 `file-b.txt` 파일이 있다. v2 버전은 히스토리에 더이상 없다.

### Checkout <a id="checkout"></a>
아마도 `checkout` 명령과 `reset` 명령에 어떤 차이가 있는지 궁금할 것이다. `reset` 명령과 마찬가지로 `checkout` 명령도 위의 세 트리를 조작한다. `checkout` 명령도 파일 경로를 쓰느냐 안 쓰느냐에 따라 동작이 다르다.

#### 경로 없음
`git checkout <branch>` 명령은 `git reset --hard <branch>` 명령과 비슷하게 `<branch>` 스냅샷을 기준으로 세 트리를 조작한다. 하지만, 두 가지 사항이 다르다.

첫 번째로 `reset --hard` 명령과는 달리 `checkout` 명령은 워킹 디렉토리를 안전하게 다룬다. 저장하지 않은 것이 있는지 확인해서 삭제하지 않는다는 것을 보장한다. 사실 보기보다 좀 더 똑똑하게 동작한다. 워킹 디렉토리에서 merge 작업을 한번 시도해보고 **변경된 적이 없는** 파일만 업데이트를 시도한다. 반면 `reset --hard` 명령은 확인하지 않고 단순히 모든 것을 바꿔버린다.

두 번째 중요한 차이점은 `checkout` 명령이 HEAD를 업데이트 하는 방법이다. `reset` 명령은 HEAD가 가리키는 브랜치를 움직이지만 (브랜치 Refs를 업데이트하지만), `checkout` 명령은 HEAD 자체를 다른 브랜치로 옮긴다.

예를 들어 각각 다른 commit을 가리키는 `main`과 `develop` 브랜치가 있고 현재 워킹 디렉토리는 `develop` 브랜치라고 가정해보자 (즉 HEAD는 `develop` 브랜치를 가리킨다). `git reset main` 명령을 실행하면 `develop` 브랜치는 `main` 브랜치가 가리키는 commit과 같은 commit을 가리키게 된다. 반면 `git checkout main` 명령을 실행하면 `develop` 브랜치가 가리키는 commit은 바뀌지 않고 HEAD가 `main` 브랜치를 가리키도록 업데이트된다. 이제 HEAD는 `main` 브랜치를 가리키게 된다.

그래서 위 두 경우 모두 HEAD는 결과적으로 `commit A`을 가리키게 되지만 방식은 완전히 다르다. `reset` 명령은 HEAD가 가리키는 브랜치의 포인터를 옮겼고 `checkout` 명령은 HEAD 자체를 옮겼다.

![fig 1-90](../images/fig-5-18.png)<br>
Fig 1-90. 
<p style='text-align: right;'><sup>(출처: https://git-scm.com/book/en/v2/Git-Tools-Reset-Demystified)</sup></p>

#### 경로 있음
`checkout` 명령을 실행할 때 파일 경로를 줄 수도 있다. `reset` 명령과 비슷하게 HEAD는 움직이지 않는다. 동작은 `git reset <branch> file` 명령과 비슷하다. index의 내용이 해당 commit 버전으로 변경될 뿐만 아니라 워킹 디렉토리의 파일도 해당 commit 버전으로 변경된다. 완전히 `git reset --hard <branch> file` 명령의 동작과 같다. 워킹 디렉토리가 안전하지도 않고 HEAD도 움직이지 않는다.

`git reset`이나 `git add` 명령처럼 `checkout` 명령도 `--patch` 옵션을 사용해서 hunk 단위로 되돌릴 수 있다.

#### 요약
`reset` 명령이 좀 더 쉬워졌을 거라고 생각한다. 아직 `checkout` 명령과 정확하게 무엇이 다른지 혼란스럽거나 정확한 사용법을 다 익히지 못했을 수도 있지만 괜찮다.

아래에 어떤 명령이 어떤 트리에 영향을 주는지에 대한 요약표를 만들었다. 명령이 HEAD가 가리키는 브랜치를 움직인다면 “HEAD” 열에 “REF”가 쓰여 있고 HEAD 자체가 움직인다면 “HEAD” 라고 적혀 있다. 'WD Safe?' 열을 주의 깊게 보자. 여기에 **NO**라고 적혀 있다면 워킹 디렉토리에 저장하지 않은 내용이 안전하지 않기 때문에 해당 명령을 실행하기 전에 한 번쯤 더 생각해보아야 한다.

|   | **HEAD** | **index** | **Workdir** | **WD Safe?** |
|---|----------|-----------|-------------|--------------|
| **Commit Level** |
| `reset ==soft <commit>` | REF | NO | NO | xxxxxYES |
| `reset <commit>` | REF | YES | NO | YES |
| `reset --hard <commit>` | REF | YES | YES | NO |
| `checkout <commit>` | HEAD | YES | YES | YES |
| **FIle Level** |
| `reset <commit> <paths> | NO | YES | NO | YES |
| `checkout <commit> <paths>` | NO | YES | YES | NO |


---
<a id="foot-1" href="#foot-1-ref"><sup>1</sup></a> 출처: [[Git] git add 취소하기, git commit 취소하기, git push 취소하기](https://gmlwjd9405.github.io/2018/05/25/git-add-cancle.html)
