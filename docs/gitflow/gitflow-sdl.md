# SDL GitfLow
이 페이지에서는 SureDataLab에서 채택하여 사용하고 Gitflow 모델에 대하여 설명하고 사용하는 방법에 대하여 설명한다.
2022년 현재 SureDataLab에서 수행하는 대부분은 고객의 데이터기반 업무 처리를 위한 빅데이터 플랫폼을 구축하는 작업이다. 데이터 모니터링, 시각화, 분석, 이상 탐지, 에측 및 추천 등이 플랫폼상의 대부분 응용이라 할 수 있다. 따라서 과업의 성격이 아직은 SI(System Integration)과 같은 특성을 띠고 있다. 즉 개발된 소프트웨어 시스템의 버전이 지속적으로 진화하는 형태가 아니고 납품한 시스템의 유지 보수, 버그 수정 등이 주로 발생한다. 따라서 소프트웨어 제품이나 서비스 제공에 따른 서비스 확장에 따른 소프트웨어 버전의 진화에 중점을 두지 않는다. 또한 프로젝트 크기 또한 소규모이므로 앞서 기술한 [Vincent Driessen 브래치 모델](../what-is-gitflow/#vincent-driessen-branching-model)를 사용하기 보다는 실정에 적합하도록 간편화한 브랜치 모델을 사용한다.

## SDL 브랜치 모델
기본적으로 분랜치 운영을 [중앙집중식 워크플로우](../../git/distributed-git/#centralized-workflows)에 기반을 두고 있다. 
세 종류의 브랜치를 사용하며 다음과 같다.

- **Main 브랜치**: develop 브랜치 없이 main 브랜치에 바로 merge한다. 이때 CI를 통해 빌드 오류가 발생하지 않도록 보장하여야 한다, 이를 위해 CI를 통해 유닛 테스트를 수행하여 항상 배포 가능한 상태로 유지한다. GitLab의 Merge Request(MR)를 통해 merge할 수 있으며, 만약 merge할 때 문제가 발생하는 경우 MR를 사용하여 revert하여 되돌릴 수 있다.
- **Issue 브랜치**: [Feature 브랜치](../what-is-gitflow/#feature-branches)와 [Hotfix 브랜치](../what-is-gitflow/#hotfix-branches)를 통합한 [토픽 브랜치](../../git/git-branch/#topic-branches)개념으로 브랜치에 대한 구분은 GitLab issue의 레이블로 한다. 이슈 브랜치 이름의 명명 규칙은 `<issue number>-<branch name>`이며, GitLab의 issue를 통해 MR를 만드는 경우 자동으로 생성된다. 개발이 완료된 후 MR를 통해 merge되면 브랜치는 삭제된다.
- **Release 브랜치**: 배포를 위한 브랜치이기는 하나 메인 브랜치로 merge하지 않는다. 배포 준비 과정 중 버그가 발견된 경우 이슈 브랜치를 통해 문제를 해결하고 배포 브랜치로 [cherry-pick](../../git/distributed-git/#rebasing-and-cherry-picking-workflows)해야 한다. 배포가 결정된 커밋은 v0.0.0 형태의 버전 태그로 배포 버전을 관리한다.

## SDL 브랜치 모델의 GitLab에 적용한 Gitflow 
여러가지 방법으로 Merge Request를 생성할 수 있으나 대표적인 것 2가지 방법을 소개한다. 하나는 [일상적으로 사용할 수 있는 방법](#git-command-locally)이며, 다른 하나는 [GitLab 페이지에서 issue를 생성하여 하는 방법](#from-an-issues)이다.

### 로컬에서 Git 명령 사용 <a id="git-commnad-locally"></a>
로컬 repository에서 git command를 수행하여 merge request를 생성할 수 있다.

1. 브랜치를 생성한다.<br>
```bash
git checkout -b <brnach-name>
```
<br>
2. 파일을 만들거나, 편집하거나 삭제한 다음, stage과 commit을 수행한다. <br>
```bash
git add .
git commit -m "Commit Message"
```
<br>
3. 브랜치를 GitLab에 push한다. <br>
```bash
git push origin <branch-name>
```
GitLab은 결과로 merge request를 생성하고 그 link를 출력한다. <br>
```
...
remote: To create a merge request for my-new-branch, visit:
remote:   https://gitlab.example.com/my-group/my-project/merge_requests/new?merge_request%5Bsource_branch%5D=my-new-branch
```
<br>
4. 위의 link(`https://gitlab.example.com/my-group/my-project/merge_requests/new?merge_request%5Bsource_branch%5D=my-new-branch`)를 GitLab을 사용하는 브라우저 페이지의 주소항에 복사한다. 

이후 `New merge reuset` 페이지로 이동한다. `Driscription`, `Assignee`, `Reviewer`, `Milestone`, `Labels`을 작성한 후, `Merge optins`에서 `Delete source ...`를 check 한 다음 `Create merge request` 버튼을 클릭하여 MR을 생성한다. 생성하면 왼쪽 `merge requests`에 MR 갯수가 증가하였음을 확인할 수 있다. 

### GitLab 페이지에서 issue 사용 <a id="from-an-issues"></a>
먼저 GitLab 페이지에서 issue를 생성한다. 페이지 왼쪽 메뉴에서 `Issues`를 클릭하고 `Lists`를 클릭하면 현재 open된 issue list가 나타나며 오른쩍 상단의 `New issue` 버튼을 클릭하여 새로운 isuue를 생성할 수 있는 `New Issue` 페이지로 이동한다. `Title`에 issue 이름을 입력하고, `Type`은 `issue`를 선택한다 (디폴트 값이 `issue`임). `Assignee`는 일반적으로 자신이며 `Due date`, `Milestone`과 `labels`를 입력한 다음 하단의 `Create Issue`를 클릭하여 issue를 생성한다. 이후 `Issues`를 클릭하고 `Lists`를 클릭하여 issue #를 확인하여 브랜치를 생성할 때 브랜치 이름에 사용하여야 한다.

위의 issue #를 사용히여 앞서 설명한 바와 같이 `<issue number>-<branch name>`의 형식으로 브랜치를 생성한다. 새 브랜치를 생성하는 과정은 아래와 같다.

1. GitLab 프로젝트 페이지의 `Repository`를 클릭하여 `Branches`를 선택하면 모든 브랜치 리스트를 보여준다.
2. 브랜치 리스트 페이지에서 오른쪽 상단의 `New branch` 버튼을 클릭하여 새로운 브랜치를 생성할 수 있는 `New Branch` 페이지로 이동한다.
3. 이 페이지에서 `Branch name`을 입력한다. 이때 `<issue number>-<branch name>`의 형식으로 이름을 입력하여야 하여 하단의 `Create branch` 버튼을 클릭하여 브랜치를 생성한다.
4. 다음, 바뀐 화면은 `Create merge Request` 버튼을 제외하면 프로젝트 페이지와 동일하다. 이때 변경하고자 하는 파일을 찾아 `Web IDE`를 이용하여 파일을 수정한 다음 commit하고 이후 그 페이지에서 commit message를 작성하고 `commit` 버튼을 클릭하여 생성된 브랜치에 commit하고 새로운 merge reauest를 시작한다.
5. `New merge request` 패이지로 이동하면, `Source branch`에서 직전에 생성한 브랜치를 선택하고 `Target Branch`에서 `main`을 선택한 다음 하단의 `Compare branches and continue` 버튼을 클릭하여 merge reauest를 생성한다.
6. 이후 다시 한번 `New merge reuset` 페이지로 이동한다. `Driscription`, `Assignee`, `Reviewer`, `Milestone`, `Labels`을 작성한 후, `Merge optins`에서 `Delete source ...`를 check 한 다음 `Create merge request` 버튼을 클릭하여 MR을 생성한다. 생성하면 왼쪽 `merge requests`에 MR 갯수가 증가하였음을 확인할 수 있다. 

### GitLab 페이지에서 Main 브랜치로 Merge  
앞 두 세션 중 하나를 수행한 다음, 앞서 작성한 merge request의 `description` 페이지로 이동하여 `approve` 버튼을 클릭하여 approve하는 것으로 실제 merge를 시작한다. 이후 다음의 `Merge` 버튼을 클릭하여 merge를 수행하면 로그가 변화된 것을 볼 수 있다. 만약 잘못한 것이어서 돌이키고 싶다면 `Revert` 버튼을 conflict가 발생하여 cherry-picking을 하려면 `Cherry-pick` 버튼을 source branch를 제거하고 싶다면 `Delete source branch` 버튼을 클릭할 수 있다.

필요하면 comment를 입력한다. 이제 merge를 완료하였다. 왼쪽 메뉴에서 `Project Information`을 클릭하고 `Activity`를 클릭하면 지금까지 수행한 작업 리스트를 확인할 수 있다.
