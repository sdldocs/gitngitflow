
SW 개발과 운영은 특별한 경우를 제외하면 대부분 여러명의 협업으로 수행된다. 특히 최근 COVID-19으로 인한 팬데믹 이후 재택 근무가 일상화되며 온라인상에서 협업은 더욱 중요하게 되었다. 이같은 상황의 핵심에는 소스 코드 버전 관리 시스템이 있다. Gitflow는 Git을 활용하여 소규모 개발팀에서 부터 대규모 공개 소프트웨어 개발 프로젝트에 이르기까지 활용할 수 있는 워크플로우라 할 수 있다. 이미 앞서 소개한 [분산환경에서의 Git](../git/distributed-git.md)에서 다양한 워크플로우를 소개하였다. 이 파트에서는 우리에게 적합하다고 생각되는 [Vincent Driessen이 제안한 브래치 모델](https://nvie.com/posts/a-successful-git-branching-model/)에 대하여 알아보고 이에 대한 예제를 통해 적용할 수 있는 기틀을 만들도록 한다.

Gitflow를 적용하기 위하여는 기본적으로 Git에 대한 적절한 지식이 필요하다. 이를 위하여 앞선 파트에서 [Git의 개념](../git/what-is-git.md), [Git 기초](../git/getting-started-git.md), [Git 브랜치](../git/git-branch.md), [Git 서버](../git/git-server.md) 및 [분산 환경에서의 Git](../git/distributed-git.md)으로 나누어 설명하였다. 또한 간단한 cheat sheet로 [git - the simple guide
](http://rogerdudler.github.io/git-guide/index.ko.html)를 추천한다.

## Vincent Driessen 브래치 모델 <a id="vincent-driessen-branching-model"></a>
Vincent Driessen이 제안하여 일부 프로젝트에 적용하였던 개발 모델을 소개한다. 이 모델은 매우 성공적이었다고 자평하였다. 주로 브랜치 전략과 릴리스 관리에 대해서만 기술한다.

![fig-11-01](../images/fig-11-01.png) <br>
Fig 2-1. Vincent Driessen 브랜치 모델 워크플로우 <a id="foot-1" href="#foot-1-ref"><sup>1</sup></a><br>
(출처: https://nvie.com/posts/a-successful-git-branching-model/)

이미 [분산환경에서의 Git](../git/distributed-git.md)에서 설명한 [Dictator and Lieutenants 워크플로우](../../git/distributed-git/#dictator-lieutenants-workflows)를 [비공개 소규모팀](../../git/distributed-git/#private-small-team)과 [비공개 대규모팀](../../git/distributed-git/#private-managed-team)에 적용한 것과 기본적으로 유사하나 사용하는 브랜치명을 구체적으로 사용한다는 점에서 실질적으로 적용할 수 있을 것이다.

모델에서 사용하는 브랜치는 크게 두 종류로 나눌 수 있다. 하나는 [long running 브랜치](#long-running-branches)이고, 다른 하나는 [supporting 브랜치](#supporting-branches)이다. 이제 각각에 대하여 좀 더 자세히 알아보자.

### Long-Running 브랜치 <a id="long-running-branches"></a>
long-running 브랜치는 소프트웨어 생명 주기 동안 유지되어야 하는 중앙 repository로서 일반적으로 `main` 브랜치와 `develop` 브랜치가 있다. 모든 Git 사용자는 `origin`의 `main` 브랜치와 친숙해야 한다. `main` 브랜치와 병렬로 `develop`라는 다른 브랜치가 존재한다.

`origin/main`는 `HEAD`의 소스 코드가 항상 *production 준비* 상태를 반영하는 long-running 브랜치이다.

`origin/develop`는 `HEAD`의 소스 코드가 항상 다음 릴리스를 위해 전달된 최신 개발 변경 사항을 반영하는 주요 브랜치이다. 일부는 이것을 "통합 브랜치(integration branch)"라고 부르기도 한다. 여기서 주기적으로 자동 빌드가 만들어진다.

`develop` 브랜치의 소스 코드가 안정적인 상태에 도달하여 릴리스 준비가 되면 모든 변경 사항을 `main`에 병합한 다음 릴리스 번호로 태그를 지정해야 한다. 이것이 구체적으로 어떻게 이루어지는 가는 나중에 더 설명할 것이다.

따라서 변경 사항이 `main`에 다시 병합될 때마다 이 릴리스는 **정의상** 새로운 프로덕션 릴리스가 된다. 이론적으로 `main`에 대한 커밋이 있을 때마다 Git hook 스크립트를 사용하여 소프트웨어를 자동으로 빌드하고 프로덕션 서버에 롤아웃할 수 있다.

### Supporting 브랜치 <a id="supporting-branches"></a>
`main` 및 `develop` 브랜치 다음으로, 개발 모델은 다양한 supporting 브랜치를 사용하여 팀 구성원 간의 병렬 개발을 지원하고, 기능을 쉽게 추적하고, 프로덕션 릴리스를 준비하고, 라이브 프로덕션 문제를 신속하게 해결할 수 있도록 지원한다. long-running 브랜치와는 달리, 이 브랜치들은 결국 삭제될 것이기 때문에 항상 제한된 수명을 갖고 있다.

사용할 수 있는 브랜치는 다음과 같다.

- Feature 브랜치 또는 [토픽 브랜치](../../git/git-branch/#topic-branches)
- Release 브랜치
- Hotfix 브랜치

각 브랜치는 특정 목적을 가지고 있으며 어떤 브랜치로부터 다른 브랜치를 분기할 수 있고, 어떤 브랜치가 다른 브랜치의에 merge되어야 하는지에 대한 엄격한 규칙이 있으며 이를 따라야 한다. 잠시 후에 그것들을 살펴볼 것이다.

기술적 관점에서 이러한 브랜치는 결코 "특별한" 것이 아니다. 브랜치 유형은 **사용 방법**에 따라 분류된다.

Gitflow에 대한 본격적인 설명에 앞서 중앙 리모트 repository, 개발자 리모트 repository와 로컬 repository의 개념에 대하여 간단히 살펴보고 계속하자. 

"중앙 리모트 repository"는 long-running 브랜치를 저장하는 곳으로 일반적으로 리모트 Git 서버이며, 프로젝트에 참여하는 모든 팀원들에게 공개된다. 프로젝트의 `main` 브랜치와 `develop` 브랜치가 이에 해당된다고 할 수 있다. "개발자 리모트 repository"는 개발자가 프로젝트 개발 동안 자신이 개발한 코드를 프로젝트 팀원들에게 공개하기 위한 repository로 공유를 위하여 리모트 서버에 저장한다. 마지막으로 "로컬 repository"는 개발자가 개발을 진행하는 동안 사용하는 작업 공간이라 할 수 있으면 공유하지 않는 자신만의 브랜치들이 있다.


## `main` 브랜치<a id="foot-1" href="#foot-1-ref"><sup>1</sup></a> <a id="main-branch"></a>
`origin/main`는 `HEAD`의 소스 코드가 항상 *production 준비* 상태를 반영하는 long-running 브랜치이다. 즉 릴리스 이력을 관리하기 위해서, 즉 릴리스 가능한 상태만을 관리한다. `develop` 브랜치의 소스 코드가 안정적인 상태에 도달하여 릴리스 준비가 되면 모든 변경 사항을 `main`에 merge 다음 릴리스 번호로 태그를 지정해야 한다. 따라서 변경 사항이 `main`에 다시 병합될 때마다 이 릴리스는 **정의상** 새로운 프로덕션 릴리스가 된다. 이론적으로 `main`에 대한 커밋이 있을 때마다 Git hook 스크립트를 사용하여 소프트웨어를 자동으로 빌드하고 프로덕션 서버에 롤아웃할 수 있다.

![fig-11-02](../images/fig-11-02.svg) <br>
Fig 2-2. `main` 브랜치와 `develop` 브랜치의 관계 <br>
(출처: https://gmlwjd9405.github.io/2018/05/11/types-of-git-branch.html)

## `develop` 브랜치 <a id="develop-branch"></a>
`origin/develop`는 `HEAD`의 소스 코드가 항상 다음 릴리스를 위해 전달된 최신 개발 변경 사항을 반영하는 long-running 브랜치이다. 일부는 이것을 "통합 브랜치(integration branch)"라고 부르기도 한다. 여기서 주기적으로 자동 빌드가 만들어진다. `develop` 브랜치의 소스 코드가 안정적인 상태에 도달하여 릴리스 준비가 되면 모든 변경 사항을 `main`에 merge한 다음 릴리스 번호로 태그를 지정해야 한다. 즉 다음 출시 버전을 개발하는 브랜치로 기능 개발을 위한 브랜치들을 병합하기 위해 사용한다. 따라서 대부분 경우, 개발 팀원들은 이 브랜치로부터 시작하고 개발 결과를 이곳에 merge한다. 새로운 기능을 개발하고, 모든 기능이 추가되고 버그가 수정되어 배포 가능한 안정적인 상태라면 `develop` 브랜치를 `main` 브랜치에 merge한다. 평소에는 이 브랜치를 기반으로 개발을 진행한다.

![fig-11-03](../images/fig-11-03.png) <br>
Fig 2-3. `develop` 브랜치를 `main` 브랜치에 merge <br>
(출처: https://gmlwjd9405.github.io/2018/05/11/types-of-git-branch.html)

## Feature 브랜치 <a id="feature-branches"></a>
feature 브랜치(또는 topic 브랜치라고도 함)는 다가오는 릴리스 또는 먼 미래의 릴리스에 대한 새로운 기능을 개발하는 데 사용된다. 기능 개발을 시작할 때, 이 기능이 통합될 대상 릴리스는 그 시점에서는 잘 알려지지 않을 수 있다. feature 브랜치의 본질은 기능이 개발 중인 기간에는 존재하지만 결국 (다음 릴리스에 새로운 기능을 확실히 추가하기 위해) `develop`로 merge되거나 (실험이 실패한 경우) 폐기된다는 것이다.

feature 브랜치는 일반적으로 개발자 로컬 repository에만 존재하며 origin에는 존재하지 않는다.

`develop` 브랜치로 부터 분기하고 `develop` 브랜치에 merge한다. 브랜치 이름은 `main`, `develop`, `release-*`와 `hotfix-*`를 제외하고 보통 feature 이름이나 모듈 이름으로 명명한다.

#### Feature 브랜치 생성 <a id="creating-feature-branch"></a>
새 기능에 대한 작업을 시작할 때는 `develop` 브랜치에서 다음과 같은 명령으로 분기한다.
```bash
$ git checkout -b myfeature develop
Switched to a new branch "myfeature"
```

#### 개발 완료된 feature 통합 <a id="incorporating-finished-feature-on-develop"></a>
완성된 기능들은 다음 릴리즈에 확실히 추가하기 위해 `develop` 브랜치에 merge하여야 한다:
```bash
$ git checkout develop
Switched to branch 'develop'
$ git merge --no-ff myfeature
Updating ea1b82a..05e9557
(Summary of changes)
$ git branch -d myfeature
Deleted branch myfeature (was 05e9557).
$ git push origin develop
```

`--no-ff` 옵션은 merge가 fast-forward를 할 수 있는 경우에도 항상 새 커밋 객체를 만들도록 한다. 이렇게 하면 feature 브랜치의 과거 존재에 대한 정보가 손실되는 것을 방지하고 기능을 추가한 모든 커밋을 함께 그룹화한다. 

![fig-11-04](../images/fig-11-04.png) <br>
Fig 2-4. feature 브랜치를 `develop` 브랜치에 merge <br>
(출처: https://gmlwjd9405.github.io/2018/05/11/types-of-git-branch.html)

위 그림에서 좌측, 즉 `--no-ff` 옵션을 사용하지 않은 경우, Git 이력에서 어떤 커밋 객체가 기능을 구현했는지 알 수 없다. 전체 기능(예: 커밋 그룹)을 되돌리는 것은 이 상황에서 진정한 골칫거리인 반면 `--no-ff` 옵션을 사용하면 쉽게 이루어질 수 있다. 몇 개의 (빈) 커밋 객체를 더 만들지만, 비용보다 훨씬 큰 이점이 있다.


## 릴리스 브랜치 <a id="release-branches"></a>
릴리스 브랜치로 새 프로덕션 릴리스의 준비를 지원한다. 이는 마지막 순간에 점을 찍는 것이라 할 수 있다. 또한 사소한 버그를 수정하고 릴리즈를 위한 (버전 번호, 빌드 날짜 등) 메타데이터를 준비할 수 있다. 릴리스 브랜치에서 이 모든 작업을 수행하며 동시에 `develop` 브랜치에서 다음 릴리스를 위한 기능을 merge할 수 있게 된다.

`develop`에서 새로운 릴리스 브랜치를 분기하는 중요한 순간은 개발이 새로운 릴리스에 (거의) 반영된 상태이다. 이 시점에서 빌드될 릴리스에 포함할 개발된 모든 기능을 merge해야 한다. 향후 또는 다음 릴리스를 대상인 기능은 포함되지 않을 수 있고, 향후 릴리스 브랜치가 분리될 때까지 기다려야 한다.

릴리스 브랜치가 시작될 때 버전 번호를 할당한다. 그 순간까지 `develop` 브랜치는 "다음 릴리스"에 대한 변경 사항을 반영했지만, 릴리스 브랜치가 시작될 때까지 "다음 릴리스"가 0.3이 될지 1.0이 될지는 불확실하다. 이러한 결정은 릴리스 브랜치가 시작될 때 이루어지며 버전 번호 범핑에 대한 프로젝트의 규칙에 따라 수행된다.

`develop` 브랜치로 부터 분기하고 `develop` 브랜치와 `main` 브랜치에 merge한다. 브랜치 이름으로 `release-*`를 사용한다.

#### 릴리스 브랜치 생성 <a id="creating-release-branch"></a>
릴리스 브랜치는 개발 브랜치에서 생성된다. 예를 들어 버전 1.1.5가 현재 프로덕션 릴리스이며 곧 대규모 릴리스가 있을 것이라고 가정해 보자. 개발 상태는 "다음 릴리스"에 대한 준비가 완료되었으며 버전 (1.1.6 또는 2.0이 아닌) 1.2가 되기로 결정했다 . 따라서 분기하여 릴리스 브랜치에 새 버전 번호를 반영하는 이름을 지정한다.
```
$ git checkout -b release-1.2 develop
Switched to a new branch "release-1.2"
$ ./bump-version.sh 1.2
Files modified successfully, version bumped to 1.2.
$ git commit -a -m "Bumped version number to 1.2"
[release-1.2 74d9424] Bumped version number to 1.2
1 files changed, 1 insertions(+), 1 deletions(-)
```

새 브랜치를 만들고 checkout한 후 버전 번호를 범프한다. `bump-version.sh`는 새로운 버전으로 반영하기 위해 작업 복사본의 일부 파일을 변경하는 가상의 셸 스크립트이다. (물론 수동 변경일 수 있다. 중요한 점은 일부 파일이 변경된다는 것이다.) 그런 다음 범핑된 버전 번호가 커밋된다.

이 새 브랜치는 릴리스가 확실히 출시될 때까지 잠시 동안 존재할 수 있다. 이 기간 동안 버그 수정은 `develop` 브랜치가 아닌 이 브랜치에 적용될 수 있다. 여기에 대규모 새 기능을 추가하는 것은 엄격히 금지된다. 그것들은 개발되어 `develop` 브랜치에 반드시 merge되어야 하며, 따라서 다음 릴리스를 기다려야 한다.

#### 릴리스 브랜치 종료 <a id="finishing-release-branch"></a>
릴리스 브랜치의 상태가 실제 릴리스가 될 준비가 되면 몇 가지 작업을 수행해야 한다. 먼저 릴리스 브랜치를 `main`에 merge한다 (`main`에 대한 모든 커밋은 정의상 새로운 릴리스임을 기억하자). 그런 다음 나중에 이 이력 버전을 쉽게 참조할 수 있도록 `main`에 대한 커밋에 태그를 지정해야 한다. 마지막으로, 릴리스 브랜치에서 변경한 내용은 향후 릴리스에서도 이러한 버그 수정을 포함하도록 다시 merge하여 개발해야 한다.

Git의 처음 두 단계:
```
$ git checkout main
Switched to branch 'main'
$ git merge --no-ff release-1.2
Merge made by recursive.
(Summary of changes)
$ git tag -a 1.2
```

이제 릴리스가 완료되었으며 나중에 참조할 수 있도록 태그가 지정되었다.

**Note**: `-s` 또는 `-u <key>` 옵션를 사용하여 태그를 암호로 서명하는 것이 바람직하다.

릴리스 브랜치의 변경 사항을 유지하려면 해당 변경 사항을 다시 개발로 병합해야 한다. Git에서:
```
$ git checkout develop
Switched to branch 'develop'
$ git merge --no-ff release-1.2
Merge made by recursive.
(Summary of changes)
```

이 단계를 수행하면 merge 충돌이 발생할 수 있다 (버전 번호를 변경했기 때문에 그럴 수도 있다). 그렇다면 이를 해결하고 커밋해야 한다.

이제 릴리스 브랜치는 더 이상 필요하지 않으므로 제거한다.
```
$ git branch -d release-1.2
Deleted branch release-1.2 (was ff452fe).
```


## Hotfix 브랜치 <a id="hotfix-branches"></a>
hotfix 브랜치는 계획되지 않았지만 새로운 프로덕션 릴리스를 준비하도록 되어 있다는 점에서 릴리스 브랜치와 매우 유사합니다. 그들은 라이브 프로덕션 버전의에서 버그와 같은 원하지 않는 상태가 발생하면 즉각적인 조치의 필요성으로부터 발생한다. 프로덕션 버전의 중요한 버그를 즉시 해결해야 하는 경우 hotfix 브랜치는 프로덕션 버전을 표시하는 `main` 브랜치의 해당 태그에서 분리한다.

본질은 다른 사람이 빠른 프로덕션 릴리스에 대한 수정을 준비하는 동안 (`develop` 브랜치에 있는) 팀원들의 작업을 지속할 수 있다는 것이다.

`main` 브랜치로 부터 분기하고 `develop` 브랜치와 `main` 브랜치에 merge한다. 브랜치 이름으로 `hotfix-*`를 사용한다.

#### Hotfix 브랜치 생성 <a id="creating-hotfix-branch"></a>
핫픽스 분기는 `main` 브랜치에서 생성된다. 예를 들어 버전 1.2가 라이브로 실행되고 심각한 버그로 인해 문제를 일으키는 현재 프로덕션 릴리스라고 가정한다. 그러나 현재 `develop` 브랜치의 상태는 아직 불안정하다. 이경우 hotfix 브랜치를 분기하여 문제 해결을 시작할 수 있다.
```
$ git checkout -b hotfix-1.2.1 main
Switched to a new branch "hotfix-1.2.1"
$ ./bump-version.sh 1.2.1
Files modified successfully, version bumped to 1.2.1.
$ git commit -a -m "Bumped version number to 1.2.1"
[hotfix-1.2.1 41e61bb] Bumped version number to 1.2.1
1 files changed, 1 insertions(+), 1 deletions(-)
```

분기한 후 버전 번호를 마추어야 한다.

그런 다음 버그를 수정하고 각각의 수정 사항에 대하여 개별 커밋으로 수정을 커밋한다.
```
$ git commit -m "Fixed severe production problem"
[hotfix-1.2.1 abbe5d6] Fixed severe production problem
5 files changed, 32 insertions(+), 17 deletions(-)
```

#### Hotfix 브랜치 종료 <a id="finishing-hotfix-branch"></a>
버그 수정이 완료되면 버그 수정은 `main`에 다시 merge되어야 하지만 버그 수정이 다음 릴리스에도 포함되도록 보장하기 위해 `develop` 브랜치에도 merge되어야 한다. 이는 릴리스 브랜치가 완료되는 방식과 완전히 동일하다.

먼저 `main` 브랜치를 업데이트하고 릴리스에 태그를 지정한다.
```
$ git checkout main
Switched to branch 'main'
$ git merge --no-ff hotfix-1.2.1
Merge made by recursive.
(Summary of changes)
$ git tag -a 1.2.1
```

**Note**: `-s` 또는 `-u <key>` 옵션를 사용하여 태그를 암호로 서명하는 것이 바람직하다.

다음으로, 버그 수정도 `develop` 브랜치에 merge 시킨다.
```
$ git checkout develop
Switched to branch 'develop'
$ git merge --no-ff hotfix-1.2.1
Merge made by recursive.
(Summary of changes)
```

여기서 규칙의 한 가지 예외는 릴리스 브랜치가 현재 삭제되지 않고 존재하는 경우 핫픽스 변경 사항을 `develop` 브랜치 대신 해당 릴리스 브랜치에 merge해야 한다. 버그 수정을 릴리스 분기에 다시 merge하고 결국 버그 수정도 `develop` 브랜치에 병합한다. (개발 중인 작업에 즉시 이 버그 수정이 필요하며 릴리스 브랜치가 완료될 때까지 기다릴 수 없다면 버그 수정도 지금 `develop` 브랜치에 안전하게 merge해야 한다.)

마지막으로 hotfix 브랜치를 제거한다.
```
$ git branch -d hotfix-1.2.1
Deleted branch hotfix-1.2.1 (was abbe5d6).
```


## 요약 <a id="summary"></a>
이 브랜치 모델에 대해 충격적인 것은 없지만, 이 페이지의 "큰 그림"은 우리의 프로젝트에 매우 유용했다고 밝혀졌다. 이해하기 쉬운 우아한 브랜치 모델을 형성하고 팀원들이 분기 및 해제 프로세스에 대한 이해를 공유할 수 있도록 한다.

## References
- [Git Workflow](https://gmlwjd9405.github.io/2018/05/12/how-to-collaborate-on-GitHub-3.html)
- [A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/)

---
<a id="foot-1" href="#foot-1-ref"><sup>1</sup></a>[GitHub to replace 'master' with 'main' starting next month](https://www.zdnet.com/article/github-to-replace-master-with-main-starting-next-month/)